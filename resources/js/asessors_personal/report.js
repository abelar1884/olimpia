/**
 * Created by Эдуард on 14.06.2019.
 */
import axios from 'axios';

$(document).ready(function () {
    new Vue({
        el: '#change_total',
        methods: {
            sendRequest: function (report, asessor, e) {
                let isDelete = confirm('Вы действительно хотите изменить итоговый результат?');
                if (isDelete)
                {
                    axios.post('/api/change-total/request-for-change', {report_id: report, asessor_id: asessor})
                        .then(function (response) {
                        if (response.data.resp)
                        {
                            alert('Запрос отправлен');
                            $(e.target).prop('disabled', true);
                        }
                    });

                }
            }
        }
    })
});