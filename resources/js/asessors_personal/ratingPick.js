import axios from 'axios';

$(document).ready(function () {
    new Vue({
        el: '#rating-pick',
        data: {
            fields: [],
            params: {
                param_1: 0,
                param_2: 0,
                param_3: 0,
                param_4: 0,
                best_practice: 0,
                my_rating: 0
            },
            example: 0,
            dateSave: false,
            total_value: 0,
            REPORT: window.location.pathname.split('/')[4],
            CRITERION: window.location.pathname.split('/')[5],
            USER: $('#asessor_id').val(),
            saveFields: [],
            idSave: null,
            errors: []
        },
        mounted() {
            let self = this;

            axios.post('/api/asessor/criteria', {id: this.CRITERION}).then(response => (this.fields = response.data));


            axios.post('/api/asessor/criteria/saved', {report_id: this.REPORT, user_id: this.USER, criterion_id: this.CRITERION})
                .then(function (response) {
                    if (response.data.id) {
                        self.params.param_1 = response.data.param_1;
                        self.params.param_2 = response.data.param_2;
                        self.params.param_3 = response.data.param_3;
                        self.params.param_4 = response.data.param_4;
                        self.params.best_practice = response.data.best_practice;
                        self.params.my_rating = response.data.rating_as_1;
                        self.dateSave = true;
                        self.idSave = response.data.id
                    }
                });


        },
        beforeUpdate() {

            let sum_params = parseFloat(this.params.param_1)*parseFloat(this.fields.param_1)
                            + parseFloat(this.params.param_2)*parseFloat(this.fields.param_2)
                            + parseFloat(this.params.param_3)*parseFloat(this.fields.param_3)
                            + parseFloat(this.params.param_4)*parseFloat(this.fields.param_4);
            let final = sum_params/3*0.8+parseFloat(this.params.best_practice);

            if (final > 1)
            {
                this.example = final.toFixed(2)-1;
            }
            else {
                this.example = final.toFixed(2);
            }

            this.total_value = parseFloat(this.params.my_rating)*this.fields.max_value;

        },
        methods: {
            resetSave: function (id) {
                let isDelete = confirm('Вы действительно хотите удалить сохранение?');
                if (isDelete)
                {
                    axios.post('/api/asessor/criteria/reset-saved', {id: id}).then();
                    location.reload();
                }
            }
        }
    });
});
