/**
 * Created by Эдуард on 16.05.2019.
 */
import axios from 'axios';

$(document).ready(function () {
    new Vue({
        el: '#add-asessors',
        data: {
            name: null,
            asessors: [],
            selectedAs: [],
            selectedName: [],
            REPORT: window.location.pathname.split('/')[4],
            CURRENT_URL: window.location.pathname
        },
        mounted() {

            if (this.CURRENT_URL == '/admin/create/report')
            {
                axios.post('/api/asessor/all').then( response => (this.asessors  = response.data) );
            }
            else {
                axios.post('/api/asessor/not-include', {report_id: this.REPORT}).then( response => (this.asessors  = response.data) );
            }

        },
        methods: {
            search: function () {
                if (this.CURRENT_URL == '/admin/create/report') {
                    axios.post('/api/asessor/search', {name: $('#search-field').val()})
                        .then(response => (this.asessors = response.data));
                }
                else {
                    axios.post('/api/asessor/search-exclude', {name: $('#search-field').val(), report_id: this.REPORT})
                        .then(response => (this.asessors = response.data));
                }
            },
            getNameAs: function (ids) {
                console.log(ids);
                axios.post('/api/asessor/get-name-by-id', {ids: ids}).then(response => (this.selectedName = response.data))
            },
            searchEmpty: function () {

                $('#search-field').val('');
                this.search();
            }
        }
    });
});