/**
 * Created by Эдуард on 06.05.2019.
 */
import axios from 'axios';

$(document).ready(function () {
    new Vue({
        el: '#delete-files',
        data: {
            JKJKJ: [],
            className: 'clajh'
        },
        methods: {
            say: function (doc) {
                let isDelete = confirm('Вы действительно хотите удалить файл?');
                if (isDelete)
                {
                    axios.post('/api/report/delete-files',{doc_id: doc}).then(response => (this.JKJKJ = response.data));
                    $('#doc-'+doc).hide();
                    alert('Файл удален');
                }

            },
            change: function (id) {
                $('#name'+id).removeAttr('disabled');
                $('#check_id'+id).prop('checked', true);
            }
        }
    });

    new Vue({
        el: '#added-asessors',
        data: {
            added: [],
            REPORT: window.location.pathname.split('/')[4],
        },
        mounted() {
            axios.post('/api/report/added-asessors', {report_id: this.REPORT}).then( (response) => {this.added = response.data});
        },
        methods: {
            delete_asessor: function (rep_id, as_id, e) {

                let isDelete = confirm('Вы действительно хотите удалить файл?');
                if (isDelete) {
                    axios.post('/api/report/delete-asessor', {
                        report: rep_id,
                        asessor: as_id
                    }).then();
                    $(e.target.closest('li')).hide();
                    alert('Асессор удален');
                }
            }
        }
    });

});
