/**
 * Created by Эдуард on 07.05.2019.
 */
$(document).ready(function () {
    $('input[name="typeUpload"]').change(function () {
        if(this.value == 'true') {
            $('#cloud').toggleClass('hide');
            $('#drive').removeClass('hide');
        }
        else {
            $('#cloud').removeClass('hide');
            $('#drive').toggleClass('hide');
        }
    })
});