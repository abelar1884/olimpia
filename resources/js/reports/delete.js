/**
 * Created by Эдуард on 14.05.2019.
 */
/**
 * Created by Эдуард on 06.05.2019.
 */
import axios from 'axios';

$(document).ready(function () {
    new Vue({
        el: '#delete',
        data: {
            responses: null
        },
        methods: {
            remove_report: function (id) {
                let isDelete = confirm('Вы действительно хотите удалить отчет?');
                if (isDelete)
                {
                    axios.post('/api/report/delete', {report_id: id}).then(response => (this.responses = response.data));
                    $('#report-'+id).hide();
                    alert('Отчет удален');
                }
            }
        }
    });
});
