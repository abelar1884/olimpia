/**
 * Created by Эдуард on 14.05.2019.
 */
/**
 * Created by Эдуард on 06.05.2019.
 */
import axios from 'axios';

$(document).ready(function () {
    new Vue({
        el: '#delete',
        data: {
            responses: null
        },
        methods: {
            remove_asessor: function (id, e) {
                let isDelete = confirm('Вы действительно хотите удалить отчет?');
                if (isDelete)
                {
                    axios.post('/api/asessor/delete', {asessor: id}).then(response => (this.responses = response.data));
                    $(e.target.closest('tr')).hide();
                    alert('Отчет удален');
                }
            }
        }
    });
});
