@extends('asessors_area.layout.app')

@section('title', $title)

@section('bread')
    <div class="bread-crumb">
        <div class="left-bread">
            <a class="btn btn-warning" href="{{route('asessor.report.allcriterion', ['report_id' => $report])}}"><i class="fas fa-arrow-left"></i></a>
            <h4>{{$title}}</h4>
        </div>
        <div class="right-bread">
            <ul>
                <li><a href="{{route('asessor.index')}}">Главная</a> <span>/</span></li>
                <li><a href="{{route('asessor.reports')}}">Отчеты</a> <span>/</span></li>
                <li><a>Оценка</a></li>
            </ul>
        </div>
    </div>
@endsection

@section('content')

    {{Form::open(['url' => '/asessor/criteria/save', 'class' => 'row', 'file' => false, 'id' => 'rating-pick'])}}

    <input id="asessor_id" name="asessor_id" value="{{$user->id}}" hidden>
    <input name="report_id" value="{{$report}}" hidden>
    <input name="criteria_id" value="{{$criterion->id}}" hidden>

    <div v-cloak class="col-xl-12">
        <div v-if="errors.length" class="alert alert-danger">
            <b>Пожалуйста исправьте указанные ошибки:</b>
            <ul>
                <li v-for="error in errors">@{{ error }}</li>
            </ul>
        </div>
    </div>

    <div class="col-xl-8" v-cloak="">
        <div class="card">
            <div class="card-row">
                <h4>{{$criterion->name}}</h4>
            </div>
            <div class="card-row" >
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th style="width: 50%;"></th>
                        <th style="width: 30%"></th>
                        <th style="width: 20%;"></th>
                    </tr>
                    </thead>
                    <tbody class="criterion-body">
                        <tr>
                            <td scope="col">Макс. баллов</td>
                            <td colspan="2" class="column-center">@{{ fields.max_value }}</td>

                        </tr>
                        <tr>
                            <td scope="col">Нормативно-регламентное  обеспечение</td>
                            <td class="column-center"><span> Вес -</span> @{{ fields.param_1 }}</td>
                            <td>
                                <input type="number" class="form-control" name="param_1" v-model="params.param_1" min="0" max="3" step="1" required>
                            </td>
                        </tr>
                        <tr>
                            <td scope="col">Информационная система управления</td>
                            <td class="column-center"><span> Вес -</span> @{{ fields.param_2 }}</td>
                            <td><input type="number" class="form-control" name="param_2" v-model="params.param_2" min="0" max="3" step="1" required></td>
                        </tr>
                        <tr>
                            <td scope="col">Артефакты</td>
                            <td class="column-center"><span> Вес -</span> @{{ fields.param_3 }}</td>
                            <td><input type="number" class="form-control" name="param_3" v-model="params.param_3" min="0" max="3" step="1" required></td>
                        </tr>
                        <tr>
                            <td scope="col">Качество процесса</td>
                            <td class="column-center"><span> Вес -</span> @{{ fields.param_4 }}</td>
                            <td><input type="number" class="form-control" name="param_4" v-model="params.param_4" min="0" max="3" step="1" required></td>
                        </tr>
                        <tr>
                            <td scope="col">Лучшая практика</td>
                            <td class="column-center"></td>
                            <td><input type="number" class="form-control" name="best_practice" v-model="params.best_practice" min="0" max="0.2" step="0.01" required></td>
                        </tr>
                        <tr>
                            <td scope="col">Расчетное значение  в долях от 1</td>
                            <td colspan="2" class="column-center">@{{ example }}</td>
                            <input name="rated" :value="example" hidden>
                        </tr>
                        <tr>
                            <td scope="col">Оценка асессора 1 по критерию  в долях от 1</td>
                            <td class="column-center"></td>
                            <td><input type="number" name="rating_as_1" min="0" max="1" step="0.01" v-model="params.my_rating" class="form-control" required></td>
                        </tr>
                        <tr hidden>
                            <td scope="col">Оценка асессора 2 по критерию  в долях от 1</td>
                            <td class="column-center"></td>
                            <td>1
                                <input class="form-control" name="rating_as_2" value="1" hidden></td>
                        </tr>
                        <tr hidden>
                            <td scope="col">Отклонение</td>
                            <td colspan="2" class="column-center">
                                0
                                <input class="form-control" name="deviation" value="1" hidden>
                            </td>

                        </tr>
                        <tr hidden>
                            <td scope="col">Скорректированная оценка  в долях от 1</td>
                            <td></td>
                            <td><input class="form-control" name="corrected" value="1" hidden></td>
                        </tr>
                        <tr>
                            <td scope="col">Оценка в баллах</td>
                            <td colspan="2" class="column-center">
                                @{{ total_value.toFixed(1) }}
                                <input class="form-control" :value="total_value.toFixed(1)" name="total" value="1" hidden>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-row">
                <input type="submit" class="btn btn-warning" value="Сохранить">
                <input v-if="dateSave" type="button" class="btn btn-danger right" v-on:click="resetSave(idSave)" value="Сбросить">
            </div>
        </div>
    </div>
    {{Form::close()}}


@endsection

@section('scripts')
    <script rel="script" src="{{asset('/js/asessors_personal/index.js')}}"></script>
    <script rel="script" src="{{asset('/js/asessors_personal/ratingPick.js')}}"></script>
@endsection
