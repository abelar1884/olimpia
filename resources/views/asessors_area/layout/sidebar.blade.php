<div class="sidebar">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3>AsessorPanel</h3>
        </div>
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{route('asessor.index')}}"><i class="fas fa-tachometer-alt"></i>Главная</a>
            </li>
            <li>
                <a href="{{route('asessor.reports')}}"><i class="fas fa-pen"></i>Отчеты</a>
            </li>
        </ul>
    </div>

</div>