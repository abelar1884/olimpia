<nav class="topbar">
    <div class="navbar-header">

        <div class="top-left-part">
            {{$user->name}}
        </div>

       <div class="top-right-part">
           <a title="Выход" style="float: right" href="" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-parent="#MainMenu">
               <i class="fas fa-power-off"></i> Выход
           </a>
           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
               @csrf
           </form>
       </div>

    </div>
</nav>