@extends('asessors_area.layout.app')

@section('title', $title)

@section('bread')
    <div class="bread-crumb">
        <div class="left-bread">

        </div>
        <div class="right-bread">
            <ul>
                <li><a>Главная</a></li>
            </ul>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-xl-4">
            <div class="card">
                <div class="card-body">
                    <h4>Персональная информация</h4>
                    <div class="card-row">
                        <h6>E-Mail</h6>
                        <input class="form-control" value="{{$user->email}}" disabled>
                    </div>
                    <div class="card-row">
                        <h6>Телефон</h6>
                        <input id="phone" class="form-control" value="{{$user->dataAs? $user->dataAs->phone: '9370991535'}}" disabled>
                    </div>
                    <div class="card-row">
                       
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4">
            <div class="card">
                <div class="card-body">
                    <h4>Ваши номинации</h4>
                    @if($user->nominationAs->first())
                        <ol>
                            @foreach($user->nominationAs as $nomination)
                                <li>{{$nomination->name}}</li>
                            @endforeach
                        </ol>
                    @else
                        Не выбрвно номинаций.
                    @endif
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script rel="script" src="{{asset('/js/asessors_personal/index.js')}}"></script>
@endsection