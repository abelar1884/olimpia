@extends('asessors_area.layout.app')

@section('title', $title)

@section('bread')
    <div class="bread-crumb">
        <div class="left-bread">
            <a class="btn btn-warning" href="{{route('asessor.reports')}}"><i class="fas fa-arrow-left"></i></a>
        </div>
        <div class="right-bread">
            <ul>
                <li><a href="{{route('asessor.index')}}">Главная</a> <span>/</span></li>
                <li><a href="{{route('asessor.reports')}}">Отчеты</a> <span>/</span></li>
                <li><a>{{$report->full_name}}</a></li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    @if($report->rating)
        <div class="row">
            <div class="col-xl-6">
                <div class="card">
                    <h6>Отправить запрос администратору на изменение итоговой оценки</h6>
                    @if(!$report->changeTotal)
                    <div class="card-row" id="change_total">
                        <button type="button" class="btn btn-warning" v-on:click="sendRequest('{{$report->id}}', '{{$user->id}}', $event)">Отправить</button>
                    </div>
                    @else
                        <div class="alert alert-warning" role="alert">
                            Запрос на изменение уже отправлен.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-xl-6">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4>Основная информация</h4>
                            <div class="card-row">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td style="font-weight: bold">Номер</td>
                                        <td>{{$report->uid}}</td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">Название</td>
                                        <td>{{$report->full_name}}</td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">Номинация</td>
                                        <td>{{$report->nomination->name}}</td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">Регион</td>
                                        <td>{{$report->regions->name}}</td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">Асессоры</td>
                                        <td>
                                            @foreach($report->asessors as $asessor)
                                                @if($asessor->id != $user->id)
                                                    {{$asessor->name}}<br>
                                                @endif
                                            @endforeach
                                            @if(count($report->asessors) == 1)
                                                Другие асессоры не назначены
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4>Прикрепленные документы</h4>
                            @if($report->medias->first())
                                <table class="table">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Формат</th>
                                        <th scope="col">Название</th>
                                        <th scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($report->medias as $file)
                                        <tr id="doc-{{$file->id}}">
                                            <td>{{$file->number}}</td>
                                            <td><img class="docs-icon" src="{{\App\Modules\Classes\FileExtension::incon($file->extension)}}"></td>
                                            <td class="change-name-files">
                                                {{$file->name}}
                                            </td>
                                            <td>
                                                <a class="btn btn-primary" href="/storage/{{$file->storage}}" download>Скачать</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                @if($report->drive_link)
                                    <a class="btn btn-success" href="{{$report->drive_link}}" target="_blank">Ссылка на облако</a>
                                @endif
                            @elseif($report->drive_link)
                                <a class="btn btn-success" href="{{$report->drive_link}}" target="_blank">Ссылка на облако</a>
                            @else
                                Нет загруженных документов
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4>Дополнительная информация</h4>
                            @foreach($fields as $key => $field)
                                <div class="card-row">
                                    <label style="font-weight: bold">{{$field}}</label>
                                    <input name="{{$key}}" value="{{$report->$key}}" class="form-control" disabled>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script rel="script" src="{{asset('/js/asessors_personal/report.js')}}"></script>
@endsection