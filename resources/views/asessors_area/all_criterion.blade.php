@extends('asessors_area.layout.app')

@section('title', $title)

@section('bread')
    <div class="bread-crumb">
        <div class="left-bread">
            <a class="btn btn-warning" href="{{route('asessor.reports')}}"><i class="fas fa-arrow-left"></i></a>
        </div>
        <div class="right-bread">
            <ul>
                <li><a href="{{route('asessor.index')}}">Главная</a> <span>/</span></li>
                <li><a href="{{route('asessor.reports')}}">Отчеты</a> <span>/</span></li>
                <li>{{$title}}</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-xl-6">
            <div class="card">
                <h4>Оценки других асессоров</h4>
                <p>{{$other_ratings}}</p>
            </div>
        </div>
        <div class="col-xl-3">
            <div class="card" style="text-align: center">
                <h4>Итоговые баллы</h4>
                <h2 style="color: #ce3a3d; font-weight: bold">{{$total}}</h2>
            </div>
        </div>
        <div class="col-xl-3">
            <div class="card" style="text-align: center">
                <h4>Максимальный балл</h4>
                <h2 style="color: #ffc107; font-weight: bold">{{$max_value}}</h2>
            </div>
        </div>
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <h4>{{$title}}</h4>
                    <div class="card-row">
                        @if($criterions->first())
                            @foreach($criterions as $criterion)
                                <div class="row" style="border-bottom: 1px solid red">
                                    <div class="col-xl-4">
                                        {{$criterion->name}}
                                    </div>
                                    <div class="col-xl-2">
                                        Оценки других асессоров
                                    </div>
                                    <div class="col-xl-2">
                                        Макс. баллов <br>{{$criterion->max_value}}
                                    </div>
                                    <div class="col-xl-2">
                                        Выставленная оценка<br>
                                        @if(Criterion::estimate($user->id, $report,$criterion->id))
                                            {{Criterion::estimate($user->id, $report,$criterion->id)->total}}
                                        @else
                                            Не выставлена
                                        @endif
                                    </div>
                                    <div class="col-xl-2">
                                        <a class="btn btn-primary right" href="{{route('asessor.report.criterion', ['report' => $report, 'criterion' => $criterion->id])}}">Выставить оценку</a>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            На текущий момент не закреплено отчетов.
                        @endif
                    </div>
                    <div class="card-row">
                        {{Form::open(['url' => '/asessor/estimate', 'class' => '', 'file' => false, 'id' => 'rating-pick'])}}
                        <input name="asessor_id" value="{{$user->id}}" hidden>
                        <input name="report_id" value="{{$report}}" hidden>
                        <input type="submit" value="Оценить" class="btn btn-success">
                        <p style="color: red; font-size: 10px;">После оценки текущего отчета, итоговые результат нельза изменить.</p>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
