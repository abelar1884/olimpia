@extends('asessors_area.layout.app')

@section('title', $title)

@section('bread')
    <div class="bread-crumb">
        <div class="left-bread">

        </div>
        <div class="right-bread">
            <ul>
                <li><a href="{{route('asessor.index')}}">Главная</a> <span>/</span></li>
                <li><a>Отчеты</a></li>
            </ul>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <h4>{{$title}}</h4>

                    @if($user->reportsAs->first())
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">id</th>
                                <th scope="col">Название</th>
                                <th scope="col">Номинация</th>
                                <th scope="col">Закрепленные асессоры</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($user->reportsAs as $report)
                                <tr {{$report->rating? 'class=report-rating': ''}}>
                                    <th scope="row">{{$report->uid}}</th>
                                    <td>{{$report->full_name}}</td>
                                    <td>{{$report->nomination->name}}</td>
                                    <td>
                                        @foreach($report->asessors as $asessor)
                                            @if($asessor->id != $user->id)
                                                {{$asessor->name}}<br>
                                            @endif
                                        @endforeach
                                        @if(count($report->asessors) == 1)
                                            Другие асессоры не назначены
                                        @endif
                                    </td>
                                    <th class="reports-information">
                                        <a href="{{route('asessor.report.single', ['id' => $report->id])}}" class="btn btn-primary">Подробнее</a>
                                        @if(!$report->rating)
                                            <a href="{{route('asessor.report.allcriterion', ['report_id' => $report->id])}}" class="btn btn-warning">Параметры оценки</a>
                                        @else
                                            <h4 class="message-final">Оценен</h4>
                                        @endif
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    @else
                        На текущий момент не закреплено отчетов.
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script rel="script" src="{{asset('/js/asessors_personal/index.js')}}"></script>
@endsection