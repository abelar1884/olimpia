@extends('personal_area.layout.app')

@section('title', $title)

@section('content-pa')
    <h2>{{$title}}</h2>
    @if($data)
    <h4>Данные</h4>
    <table class="table">
        <tbody>
        @foreach($fields as $key => $field)
            <tr>
                <th scope="row">{{$field}}</th>
                <td>{{$data->$key}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @else
        Нет данных
    @endif
    @if($files->first())
        <h4>Файлы</h4>
        @foreach($files as $file)
            <a href="/storage/{{$file->storage}}" target="_blank">{{$file->name}}</a><br>
        @endforeach
    @else
        <b>Нет загруженных документов</b>
    @endif
@endsection