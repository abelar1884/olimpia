@extends('personal_area.layout.app')

@section('title', $title)

@section('content-pa')
    <h2>{{$title}}</h2>
    {{Form::open(['url' => '/pa/organization/store', 'class' => 'col-xl-12', 'file' => true])}}
    @foreach($fields as $key => $field)
        <div class="form-group">
            <label>{{$field}}</label>
            <input name="{{$key}}" class="form-control" value="{{$values? $values->$key: ''}}" required>
        </div>
    @endforeach
    <div class="form-group">
        @if($files)
            <h5>Загруженные файлы</h5>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col">Формат</th>
                    <th style="color: red" scope="col">Удалить</th>
                </tr>
                </thead>
                <tbody>
                @foreach($files as $file)
                <tr>
                    <th scope="row">{{$file->id}}</th>
                    <td>{{explode('.',$file->name)[0]}}</td>
                    <td>{{pathinfo($file->storage)['extension']}}</td>
                    <td style="padding-left: 40px;"><input type="checkbox" name="delete_files[]" value="{{$file->id}}|{{$file->storage}}"></td>
                </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>
    <div class="form-group">
        <label>Разрешенные форматы файлов: .mp4, .doc, .docx, .xls, .xlsx, .pdf.</label>
        <input name="files[]" type="file" accept=".mp4, .doc, .docx, .xls, .xlsx, .pdf" value="загрузить файлы" multiple>
    </div>
    <div class="form-group">
        <input class="btn btn-success" type="submit" value="Сохранить">
    </div>
    {{Form::close()}}
@endsection