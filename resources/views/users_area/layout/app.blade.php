@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="main-header">Личный кабинет</h1>
        <div class="row">
            <div class="col-xl-4">
                @include('personal_area.layout.sidebar')
            </div>
            <div class="col-xl-8">
                @yield('content-pa')
            </div>
        </div>
    </div>
@endsection