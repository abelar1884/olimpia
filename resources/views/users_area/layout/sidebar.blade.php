<div class="sidebar" id="MainMenu">
    <div class="list-group panel">
        <a href="{{route('pa.index')}}" class="list-group-item list-group-item-success" data-parent="#MainMenu">Главная</a>
        <a href="#demo1" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">Сведения об организации <i class="fa fa-caret-down"></i></a>
        <div class="collapse" id="demo1">
            <a href="{{route('pa.org')}}" class="list-group-item">Посмотреть</a>
            <a href="{{route('pa.org.update')}}" class="list-group-item">Внести данные об организации</a>
        </div>
        <a href="{{route('pa.index')}}" class="list-group-item list-group-item-success" style="color: red" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-parent="#MainMenu">Выход</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</div>
