@extends('admin_area.layout.app')

@section('title', $title)

@section('bread')
    <div class="bread-crumb">
        <div class="left-bread">

        </div>
        <div class="right-bread">
            <ul>
                <li><a href="{{route('admin.index')}}">Главная</a> <span>/</span></li>
                <li><a>Критерии оценки</a></li>
            </ul>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-xl-6">
            <div class="card">
                <div class="card-row">
                    <h4>{{$title}}</h4>
                </div>
                <div class="card-row">
                    @if($fields)
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">id</th>
                                <th scope="col">Название</th>
                                <th scope="col">Макс. баллов</th>
                                <th scope="col"></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($fields as $field)
                                <tr id="report-{{$field->id}}">
                                    <th scope="row">{{$field->id}}</th>
                                    <td>{{$field->name}}</td>
                                    <td>{{$field->max_value}}</td>
                                    <td>
                                        <a onclick="event.preventDefault(); document.getElementById('delete{{$field->id}}').submit();" style="cursor: pointer; color: red; font-weight: bold">Удалить</a>
                                        <form id="delete{{$field->id}}" action="{{ route('admin.fira.delete', ['id' => $field->id]) }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <b>нет полей</b>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-xl-4" hidden>
            <div class="card">
                <div class="card-row">
                    <h4>Группировка</h4>
                </div>
            </div>
        </div>

        <div class="col-xl-4">
            <div class="card">
                <div class="card-row">
                    <h4>Добавить критерий</h4>
                </div>
                <div class="card-row">
                    {{Form::open(['url'=>'/admin/criterion/store', 'class'=>'col-xl-12','file'=>false])}}
                    <div class="form-group">
                        <label>Название</label>
                        <input class="form-control" type="text" name="name" placeholder="Название">
                    </div>
                    <div class="form-group">
                        <label>Описание</label>
                        <textarea name="desc" class="form-control" rows="10" placeholder="Описание критерия" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Максимальное значение</label>
                        <input class="form-control" type="number" name="max_value" min="1" placeholder="Макс. баллов">
                    </div>
                    @foreach($params as $key => $param)
                    <div class="form-group">
                        <label>{{$param}}</label>
                        <p style="font-size: 10px; color: #a8a8a8; margin-bottom: 0">Вес должен быть от 0 до 1</p>
                        <input class="form-control" type="number" name="{{$key}}" placeholder="Укажиет вес" step="0.01" min="0" max="1">
                    </div>
                    @endforeach
                    <div class="form-group">
                        <input type="submit" value="Создать" class="btn btn-success">
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
@endsection
