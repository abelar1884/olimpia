<div class="sidebar">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3>AdminPanel</h3>
        </div>
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{route('admin.report')}}"><i class="fas fa-pen"></i>Отчеты</a>
            </li>
            <li>
                <a href="{{route('admin.criterion')}}"><i class="fas fa-table"></i>Поля оценки</a>
            </li>
            <li>
                <a href="{{route('admin.asessor')}}"><i class="fas fa-star-half-alt"></i>Асессоры</a>
            </li>
            <li>
                <a href="{{route('change.index')}}"><i class="fas fa-exclamation-triangle"></i>Изменение итогов</a>
            </li>
        </ul>
    </div>

</div>