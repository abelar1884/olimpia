@extends('admin_area.layout.app')

@section('title', $title)

@section('content-pa')

    <div class="row">
        <div class="col-xl-8">
            <h2>Группы асессоров</h2>
        </div>
        {{Form::open(['url'=>'/admin/group/add', 'class'=>'col-xl-4 add-group','file'=>false])}}
        <input type="text" class="form-control" placeholder="Название группы" name="name">
        <input type="text" value="0" name="header" hidden>
        <input type="submit" value="Создать" class="btn btn-primary right">
        {{Form::close()}}
    </div>

    <div class="row">
        @if($groups)
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col">Старший</th>
                    <th scope="col"></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($groups as $group)
                    <tr>
                        <th scope="row">{{$group->id}}</th>
                        <td>{{$group->name}}</td>
                        <td>{{$group->header? $group->headers->name: 'Старший не назначен'}}</td>
                        <td><a href="{{route('admin.group.single',['id'=>$group->id])}}">Подробнее</a></td>
                        <td><a style="color: red; font-weight: bold" href="{{route('admin.group.delete',['object'=>$group->id])}}">Удалить</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$groups->render()}}
        @else
            <b>нет пользователей</b>
        @endif
    </div>
@endsection