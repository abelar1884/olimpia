@extends('admin_area.layout.app')

@section('title', $title)

@section('content-pa')
    <div class="form-group">
        <h2>Группа - {{$group->name}}</h2>
    </div>
    {{Form::open(['url'=>'/admin/group/update/'.$group->id,'class'=>'class','file'=>false])}}
    <div class="form-group">
        <h4>Название группы</h4>
        <input class="form-control" type="text" value="{{$group->name}}" name="name">
    </div>

    <div class="form-group">
        <h4>Добавить асессора в группу</h4>
    </div>

    <div class="form-group">
        <div class="asessor-list">
            <ul id="asessor-list">
                <input type="checkbox" v-model="pick" v-bind:value="asessor.a">
            </ul>
        </div>
    </div>
    <div id="testing">
        <p>@{{ message }}</p>
        <p>
            <input v-model="message">
        </p>
        <button v-on:click="eventses" class="btn">
            Отправить
        </button>
    </div>

    <div class="form-group">
        <h4>Назначить старшего</h4>
        <h6>Текущий старший: {{$group->header != 0? $group->headers->name: 'не назначен'}}</h6>
        @if(count($thisAsessors))
            <select class="form-control" name="header">
                <option value="0">-----</option>
                @foreach($thisAsessors as $thisAsessor)
                    <option value="{{$thisAsessor->id}}">{{$thisAsessor->name}}</option>
                @endforeach
            </select>
        @else
            В группу не добавлен не один асессор
        @endif
    </div>

    <div class="form-group">
        <input type="submit" value="Сохранить">
    </div>
    {{Form::close()}}
@endsection