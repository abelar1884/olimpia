@extends('admin_area.layout.app')

@section('title', $title)

@section('content-pa')

    <div class="row">
        <div class="col-xl-8">
            <h2>Ссылки для регистрации асессоров</h2>
        </div>
        {{Form::open(['url'=>'/admin/asessor/store', 'class'=>'col-xl-4 add-group','file'=>false])}}
        <input type="email" class="form-control" placeholder="Введите e-mail" name="email">
        <input type="submit" value="Отправить" class="btn btn-primary right">
        {{Form::close()}}
    </div>

    <div class="row">
        @if($links)
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">E-Mail</th>
                    <th scope="col">Активирован</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($links as $link)
                    <tr>
                        <th scope="row">{{$link->id}}</th>
                        <td>{{$link->email}}</td>
                        <td>{{$link->activated? 'Да': 'Нет'}}</td>
                        <td>
                            <a onclick="event.preventDefault(); document.getElementById('delete{{$link->id}}').submit();" style="cursor: pointer; color: red; font-weight: bold">Удалить</a>
                            <form id="delete{{$link->id}}" action="{{ route('admin.asessor.deletelink', ['id' => $link->id]) }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$links->render()}}
        @else
            <b>нет пользователей</b>
        @endif
    </div>
@endsection