@extends('admin_area.layout.app')

@section('title', $title)

@section('content-pa')

    <div class="row">
        <div class="col-xl-8">
            <h2>{{$title}}</h2>
        </div>
        <div class="col-xl-4">
            <a href="{{route('admin.asessor')}}" class="btn btn-primary right">Назад</a>
        </div>
        {{Form::open(['url'=>'', 'class'=>'col-xl-12 add-group','file'=>false])}}
        <div class="form-group">
            <h5>Имя</h5>
            <input class="form-control" value="{{$object->name}}" disabled>
        </div>
        <div class="form-group">
            <h5>E-Mail</h5>
            <input class="form-control" value="{{$object->email}}" disabled>
        </div>
        <div class="form-group">
            <h5>Телефон</h5>
            <input class="form-control" value="{{$object->dataAs->phone}}" disabled>
        </div>
        @if($object->nominationAs)
            <div class="form-group">
                <h5>Номинации</h5>
                <ol>
                    @foreach($object->nominationAs as $nomination)
                        <li>{{$nomination->name}}</li>
                    @endforeach
                </ol>
            </div>
        @endif
        <div class="form-group">
            <h5>Отчеты закрепленные за асессором</h5>
            @if($object->reportsAs)
                <ol>
                    @foreach($object->reportsAs as $report)
                        <li><a href="{{route('admin.report.edit',['id' => $report->id])}}">{{$report->full_name}}</a></li>
                    @endforeach
                </ol>
            @else
                Нет закрепленных отчетов
            @endif
        </div>
        {{Form::close()}}
    </div>
@endsection