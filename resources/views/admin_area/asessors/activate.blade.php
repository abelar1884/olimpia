@extends('layouts.app')

@section('title', $title)

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if($key)
            <div class="row">
            {{Form::open(['url' => '/asessor/activate/store/'.$key->id.'', 'class' => 'offset-xl-3 col-xl-6', 'file' => false])}}
                <h2 style="text-align: center; margin-bottom: 30px;">Регистрация асессора</h2>
                <div class="form-group">
                    <input type="email" class="form-control" value="{{$key->email}}" disabled>
                </div>
                <div class="form-group" hidden>
                    <input type="email" name="email" class="form-control" value="{{$key->email}}">
                </div>
                <div class="form-group">
                    <input name="name" class="form-control" placeholder="ФИО">
                </div>
                <div class="form-group">
                    <input id="phone" class="form-control" name="phone" placeholder="Номер телефона">
                </div>
                <div class="form-group">

                        @foreach($nominations as $key => $nomination)
                            <div class="">
                                <h5>{{$key}}</h5>
                                <ul style="list-style-type: none; padding-left: 0">
                                    @foreach($nomination as $name)
                                        <li><input style="margin-right: 5px;" type="checkbox" name="nominations[]" value="{{$name->id}}">{{$name->name}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach

                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Пароль">
                </div>
                <div class="form-group">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Повторите пароль">
                </div>
                <div class="form-group">
                    <input type="checkbox" name="agree">
                    Я ознакомлен с <a href="{{route('agree')}}">политикой конфиденциальности</a>, и даю согдасие на обработку моих персональных данных.
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Сохранить" style="display: block;margin: 0 auto">
                </div>
            {{Form::close()}}
            </div>
        @else
            <h5 style="margin: 20px; text-align: center">Ссылка не действительна</h5>
        @endif
    </div>
@endsection

@section('scripts')
    <script rel="script" src="{{asset('/js/asessors_personal/index.js')}}"></script>
@endsection