@extends('admin_area.layout.app')

@section('title', $title)

@section('content-pa')

    <div class="row">
        <div class="col-xl-8">
            <h2>Асессоры</h2>
        </div>
        <div class="col-xl-4">
            <a class="btn btn-success right" href="{{route('admin.asessor.create')}}">Добавить асессора</a>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-row">
                    <div class="row">
                        <div class="col-xl-8">
                            <h2>Асессоры</h2>
                        </div>
                        <div class="col-xl-4">
                            <a class="btn btn-success right" href="{{route('admin.asessor.create')}}">Добавить асессора</a>
                        </div>
                    </div>
                </div>
                <div class="card-row">
                    @if($asessors)
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">ФИО</th>
                                <th scope="col">E-Mail</th>
                                <th scope="col">Закрепленные отчеты</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="delete">
                            @foreach($asessors as $asessor)
                                <tr>
                                    <th scope="row">{{$asessor->id}}</th>
                                    <td>{{$asessor->name}}</td>
                                    <td>{{$asessor->email}}</td>
                                    <td>
                                        @if($asessor->reportsAs->first())
                                            @foreach($asessor->reportsAs as $report)
                                                <a href="{{route('admin.report.edit',['id' => $report->id])}}">{{$report->uid}}</a>,
                                            @endforeach
                                        @else
                                            Нет закрепленных отчетов
                                        @endif
                                    </td>
                                    <td><a href="{{route('admin.asessor.single', ['id' => $asessor->id])}}" style="font-weight: bold">Подробнее</a></td>
                                    <td><a class="delete-link" v-on:click="remove_asessor({{$asessor->id}}, $event)">Удалить</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$asessors->render()}}
                    @else
                        <b>нет пользователей</b>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script rel="script" src="{{asset('/js/asessors/delete.js')}}"></script>
@endsection