@extends('admin_area.layout.app')

@section('title', $title)

@section('bread')
    <div class="bread-crumb">
        <div class="left-bread">

        </div>
        <div class="right-bread">
            <ul>
                <li><a href="{{route('admin.index')}}">Главная</a> <span>/</span></li>
                <li><a>Все заявки</a></li>
            </ul>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-row">
                    <div class="row">
                        <div class="col-xl-6">
                            <h2>{{$title}}</h2>
                        </div>
                        <div class="col-xl-6">
                            <a class="btn btn-primary right" href="{{route('admin.report.create')}}">Добавить отчет</a>
                        </div>
                    </div>
                </div>
                <div class="card-row">
                    @if($requests)
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">id</th>
                                <th scope="col">Асессор</th>
                                <th scope="col">Отчет</th>
                                <th scope="col">Оценка</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody id="delete">
                            @foreach($requests as $request)
                                <tr>
                                    <td>{{$request->id}}</td>
                                    <td>{{$request->asessor->name}}</td>
                                    <td>{{$request->report->full_name}}</td>
                                    <td>{{$request->report->rating->total_value}}</td>
                                    <td><input type="button" class="btn btn-warning" value="Сбросить"></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <b>нет пользователей</b>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script rel="script" src="{{asset('/js/reports/delete.js')}}"></script>
@endsection