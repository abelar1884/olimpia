@extends('admin_area.layout.app')

@section('title', $title)

@section('content-pa')
    <div class="row">
        <div class="col-xl-8">
            <h2>{{$user->name}} | {{$user->email}}</h2>
            <h5>Данные компании</h5>
        </div>
        <div class="col-xl-4">
            <a href="{{route('admin.index')}}" style="float: right" class="btn btn-primary">Назад</a>
        </div>
    </div>
    @if($user->data)
        <table class="table">
            <tbody>
            @foreach($fields as $key => $field)
                <tr>
                    <th scope="row">{{$field}}</th>
                    <td>{{$user->data->$key}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <b>Полльзователь не заполнял данные</b>
    @endif

    @if($files->first())
        <h5>Загруженные файлы</h5>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Название</th>
                <th scope="col">Фрмат</th>
                <th style="color: red" scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($files as $file)
                <tr>
                    <th scope="row">{{$file->id}}</th>
                    <td>{{explode('.',$file->name)[0]}}</td>
                    <td>{{pathinfo($file->storage)['extension']}}</td>
                    <td><a href="/storage/{{$file->storage}}">Скачать</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection