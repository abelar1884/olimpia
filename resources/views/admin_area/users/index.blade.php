@extends('admin_area.layout.app')

@section('title', $title)

@section('content-pa')
    @if($users)
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">E-Mail</th>
                <th scope="col">Название организации</th>
                <th scope="col">Указаны данные компании</th>
                <th style="color: red" scope="col">Подробнее</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{$user->id}}</th>
                    <td>{{$user->email}}</td>
                    <td>{{$user->data? $user->data->full_name: 'Данные не указаны'}}</td>
                    <td>{{$user->data? 'Да': 'Нет'}}</td>
                    <td><a href="{{route('admin.user.single',['id'=>$user->id])}}">Подробнее</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$users->render()}}
    @else
        <b>нет пользователей</b>
    @endif
@endsection