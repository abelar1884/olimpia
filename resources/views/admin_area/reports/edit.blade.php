@extends('admin_area.layout.app')

@section('title', $title)

@section('bread')
    <div class="bread-crumb">
        <div class="left-bread">
            <a href="{{route('admin.report')}}" class="btn btn-warning right"><i class="fas fa-arrow-left"></i></a>
        </div>
        <div class="right-bread">
            <ul>
                <li><a href="{{route('admin.index')}}">Главная</a> <span>/</span></li>
                <li><a href="{{route('admin.report')}}">{{$title}}</a> <span>/</span></li>
                <li><a>{{$title}}</a></li>
            </ul>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-xl-6">
            <div class="card">
                <div class="card-row">
                    <h2>{{$title}}</h2>
                </div>

                <div class="card-row">
                    {{Form::open(['url'=>'/admin/report/update/'.$object->id,'class'=>'col-xl-12','file'=>true])}}
                    <div class="form-group">
                        <label style="font-weight: bold">Номер</label>
                        <input type="number" name="uid" class="form-control" value="{{$object->uid}}" required>
                        <span style="color: red; font-weight: bold">Это поле обязательно для заполнения</span>
                    </div>
                    <div class="form-group">
                        <label style="font-weight: bold">Номинация</label>
                        <select name="nomination_id" class="form-control" type="number">
                            <option value="null">----------Выбрать номинацию----------</option>
                            @foreach($nominations as $key => $nomination)
                                <optgroup label="{{$key}}">
                                    @foreach($nomination as $item)
                                        @if($item->id == $object->nomination_id)
                                            <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                        @else
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                        <span style="color: red; font-weight: bold">Это поле обязательно для заполнения</span>
                    </div>
                    <div class="form-group">
                        <label style="font-weight: bold">Полное наименование организации</label>
                        <input class="form-control" name="full_name" value="{{$object->full_name}}">
                        <span style="color: red; font-weight: bold">Это поле обязательно для заполнения</span>
                    </div>
                    <div class="form-group">
                        <select name="region" class="form-control">
                            @foreach($regions as $region)
                                @if($object->region == $region->id)
                                    <option selected value="{{$region->id}}">{{$region->name}}</option>
                                @else
                                    <option value="{{$region->id}}">{{$region->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    @foreach($fields as $key => $field)
                        <div class="form-group">
                            <label style="font-weight: bold">{{$field}}</label>
                            <input name="{{$key}}" value="{{$object->$key}}" class="form-control">
                        </div>
                    @endforeach
                    <div class="form-group" id="add-asessors">
                        <label style="font-weight: bold">Добавить асессоров <span style="color: red">(максимум 5)</span></label>
                        <div class="search-asessor">
                            <input class="form-control" placeholder="Введите имя" id="search-field">
                            <button title="Найти" type="button" class="btn btn-primary" v-on:click="search()"><i class="fas fa-search"></i></button>
                            <button type="button" class="btn search-empty" v-on:click="searchEmpty()"><i class="fas fa-times"></i></button>
                        </div>

                        <h6>Список асессоров</h6>
                        <div class="asessor-list">
                            <ul>
                                <li v-for="item in asessors"><input v-on:change="getNameAs(selectedAs)" v-model="selectedAs" type="checkbox" :value="item.id">@{{ item.name }}</li>
                            </ul>
                        </div>
                        <input name="asessors" :value="selectedAs" hidden>
                        <h6 style="margin-top: 10px;">Новые асессоры</h6>
                        <ul class="selected-asessors-name">
                            <li v-for="item in selectedName">@{{ item.name }}</li>
                        </ul>
                    </div>
                    <div class="form-group" id="added-asessors">
                        <label style="font-weight: bold">Асессоры закрепленные за этим отчетом</label>
                        <ul class="selected-asessors-name">
                            <li v-for="item in added" v-bind:id="item.id">@{{ item.name }}<button title="Удалить" type="button" class="btn" v-on:click="delete_asessor('{{$object->id}}',item.id, $event)"><i class="fas fa-times"></i></button> </li>
                        </ul>
                    </div>
                    <div class="form-group">
                        <h4>Загруженные документы</h4>
                        <div class="row" id="delete-files">
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Формат</th>
                                    <th scope="col">Название</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($object->medias as $file)
                                    <tr id="doc-{{$file->id}}">
                                        <td>{{$file->number}}</td>
                                        <td><img class="docs-icon" src="{{\App\Modules\Classes\FileExtension::incon($file->extension)}}"></td>
                                        <td class="change-name-files">
                                            <input id="name{{$file->id}}" name="file_name_{{$file->id}}" value="{{$file->name}}" disabled class="form-control">
                                            <button type="button" class="btn" title="Изменить название файла" v-on:click="change({{$file->id}})"><i class="fas fa-pen"></i></button>
                                            <input id="check_id{{$file->id}}" type="checkbox" name="file_name_ids[]" value="{{$file->id}}" hidden>
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="/storage/{{$file->storage}}" download>Скачать</a>
                                            <button type="button" v-on:click="say('{{$file->id}}')" class="btn btn-danger">Удалить</button>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label style="font-weight: bold">Ссылка на облако</label>
                        <input class="form-control" name="drive_link" value="{{$object->drive_link}}">
                    </div>
                    <div class="form-group">
                        <span>Загружать файлы можно в формате: pdf, doc, docx, xls, xlsx, zip, rar.</span><br>
                        <label style="font-weight: bold">Добавить документы</label><br>
                        <input type="file" name="files[]" multiple accept=".mp4, .doc, .docx, .xls, .xlsx, .pdf, .zip, .rar">
                    </div>
                    <div class="form-group">
                        <input class="btn btn-success" type="submit" value="Сохранить">
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script src="{{asset('/js/reports/add-asessors.js')}}"></script>
    <script src="{{asset('/js/reports/edit.js')}}"></script>
@endsection