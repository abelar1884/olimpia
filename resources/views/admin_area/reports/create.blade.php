@extends('admin_area.layout.app')

@section('title', $title)

@section('bread')
    <div class="bread-crumb">
        <div class="left-bread">
            <a href="{{route('admin.report')}}" class="btn btn-warning right"><i class="fas fa-arrow-left"></i></a>
        </div>
        <div class="right-bread">
            <ul>
                <li><a href="{{route('admin.index')}}">Главная</a> <span>/</span></li>
                <li><a href="{{route('admin.report')}}"></a> <span>/</span></li>
                <li><a>{{$title}}</a></li>
            </ul>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-xl-6">
            <div class="card">
                <div class="card-row">
                    <h4>{{$title}}</h4>
                </div>
                <div class="card-row">
                    {{Form::open(['url'=>'/admin/store/report','class'=>'col-xl-12','file'=>true])}}
                    <div class="form-group">
                        <label style="font-weight: bold">Номер</label>
                        <input type="number" name="uid" class="form-control" value="{{old('uid')}}" required>
                        <span style="color: red; font-weight: bold">Это поле обязательно для заполнения</span>
                    </div>
                    <div class="form-group">
                        <label style="font-weight: bold">Номинация</label>
                        <select name="nomination_id" class="form-control" type="number">
                            <option value="null">----------Выбрать номинацию----------</option>
                            @foreach($nominations as $key => $nomination)
                                <optgroup label="{{$key}}">
                                    @foreach($nomination as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                        <span style="color: red; font-weight: bold">Это поле обязательно для заполнения</span>
                    </div>
                    <div class="form-group">
                        <label style="font-weight: bold">Полное наименование организации</label>
                        <input class="form-control" name="full_name" value="{{old('full_name')}}" required>
                        <span style="color: red; font-weight: bold">Это поле обязательно для заполнения</span>
                    </div>
                    <div class="form-group">
                        <select name="region" class="form-control">
                            <option value="null">----------Выбрать регион----------</option>
                            @foreach($regions as $region)
                                <option value="{{$region->id}}">{{$region->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    @foreach($fields as $key => $field)
                        <div class="form-group">
                            <label style="font-weight: bold">{{$field}}</label>
                            <input name="{{$key}}" value="{{old($key)}}" class="form-control">
                        </div>
                    @endforeach
                    @if($asessors)
                        <div class="form-group" id="add-asessors">
                            <label style="font-weight: bold">Добавить асессоров <span style="color: red">(максимум 5)</span></label>
                            <div class="search-asessor">
                                <input class="form-control" placeholder="Введите имя" id="search-field">
                                <button title="Найти" type="button" class="btn btn-primary" v-on:click="search()"><i class="fas fa-search"></i></button>
                                <button type="button" class="btn search-empty" v-on:click="searchEmpty()"><i class="fas fa-times"></i></button>
                            </div>

                            <h6>Список асессоров</h6>
                            <div class="asessor-list">
                                <ul>
                                    <li v-for="item in asessors"><input v-on:change="getNameAs(selectedAs)" v-model="selectedAs" type="checkbox" :value="item.id">@{{ item.name }}</li>
                                </ul>
                            </div>
                            <input name="asessors" :value="selectedAs" hidden>
                            <h6 style="margin-top: 10px;">Выбранные асессоры</h6>
                            <ul class="selected-asessors-name">
                                <li v-for="item in selectedName">@{{ item.name }}</li>
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <label>Загрузить с диска</label>
                        <input name="typeUpload" value="true" type="radio" checked>

                        <label>Указать ссылку на облако</label>
                        <input name="typeUpload" value="false" type="radio">

                        <div class="input-upload">
                            <div id="drive">
                                <span>Загружать файлы можно в формате: pdf, doc, docx, xls, xlsx, zip, rar</span><br>
                                <input id="drive" type="file" name="files[]" class="" accept=".mp4, .doc, .docx, .xls, .xlsx, .pdf, .zip, .rar" multiple><br>
                            </div>
                            <div class="hide" id="cloud">
                                <label style="font-weight: bold">Ссылка на облако</label>
                                <input name="drive_link" class="form-control">
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <input class="btn btn-success" type="submit">
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('/js/selectUploadDocs.js')}}"></script>
    <script src="{{asset('/js/reports/add-asessors.js')}}"></script>
@endsection