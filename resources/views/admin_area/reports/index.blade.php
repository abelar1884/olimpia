@extends('admin_area.layout.app')

@section('title', $title)

@section('bread')
    <div class="bread-crumb">
        <div class="left-bread">

        </div>
        <div class="right-bread">
            <ul>
                <li><a href="{{route('admin.index')}}">Главная</a> <span>/</span></li>
                <li><a>{{$title}}</a></li>
            </ul>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-row">
                    <div class="row">
                        <div class="col-xl-6">
                            <h2>{{$title}}</h2>
                        </div>
                        <div class="col-xl-6">
                            <a class="btn btn-primary right" href="{{route('admin.report.create')}}">Добавить отчет</a>
                        </div>
                    </div>
                </div>
                <div class="card-row">
                    @if($reports)
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">id</th>
                                <th scope="col">Участник</th>
                                <th scope="col">Регион</th>
                                <th scope="col">Номинация</th>
                                <th scope="col"></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="delete">
                            @foreach($reports as $document)
                                <tr id="report-{{$document->id}}">
                                    <th scope="row">{{$document->uid}}</th>
                                    <td>{{$document->full_name}}</td>
                                    <td>{{$document->regions->name}}</td>
                                    <td>{{$document->nomination->name}}</td>
                                    <td><a style="color: blue; font-weight: bold" href="{{route('admin.report.edit', ['id'=>$document->id])}}">Изменить</a></td>
                                    <td>
                                        <a v-on:click="remove_report({{$document->id}})" style="cursor: pointer; color: red; font-weight: bold">Удалить</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$reports->render()}}
                    @else
                        <b>нет пользователей</b>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script rel="script" src="{{asset('/js/reports/delete.js')}}"></script>
@endsection