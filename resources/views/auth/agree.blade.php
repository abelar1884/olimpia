@extends('layouts.app')

@section('title', $title)

@section('content')
    <h1 class="main-header">{{$title}}</h1>
@endsection