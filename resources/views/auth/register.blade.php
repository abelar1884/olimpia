@extends('layouts.app')

@section('title', $title)

@section('content')
    <div class="container">
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{session('success')}}
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <h1 class="main-header">{{$title}}</h1>
        {{Form::open(['url'=>'/registered','file'=>true,'class'=>'offset-xl-3 col-xl-6'])}}
        <div class="form-group">
            <input class="form-control" name="name" placeholder="Имя">
        </div>
        <div class="form-group">
            <input class="form-control" name="email" placeholder="E-Mail" type="email">
        </div>
        <div class="form-group">
            <input class="form-control" name="password" placeholder="Пароль" type="password">
        </div>
        <div class="form-group">
            <input class="form-control" name="password_confirmation" placeholder="Повторите пароль" type="password">
        </div>
        <div class="form-group">
            <input type="checkbox" name="agree">
                Я ознакомлен с <a href="{{route('agree')}}">политикой конфиденциальности</a>, и даю согдасие на обработку моих персональных данных.
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-xl-6">
                    <input type="submit" value="Зарегистрироваться" class="btn btn-success">
                </div>
                <div class="col-xl-6">
                    <a href="{{route('login')}}" style="float: right" class="btn btn-primary">Войти</a>
                </div>
            </div>
        </div>
        {{Form::close()}}
    </div>
@endsection