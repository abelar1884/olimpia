<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/report/delete', 'Admin\Api\ReportController@delete');
Route::post('/report/delete-files', 'Admin\Api\ReportController@deleteFiles');
Route::post('/report/added-asessors', 'Admin\Api\ReportController@addedAsessors');
Route::post('/report/delete-asessor', 'Admin\Api\ReportController@deleteAsessor');



Route::post('/asessor/all', 'Admin\Api\AsessorController@getAll');
Route::post('/asessor/not-include', 'Admin\Api\AsessorController@getNotIncludeReport');
Route::post('/asessor/delete', 'Admin\Api\AsessorController@delete');
Route::post('/asessor/search', 'Admin\Api\AsessorController@search');
Route::post('/asessor/search-exclude', 'Admin\Api\AsessorController@searchExclude');
Route::post('/asessor/get-name-by-id', 'Admin\Api\AsessorController@getNameById');

Route::post('/asessor/criteria', 'AsessorPersonal\ApiController@getCriterion');
Route::post('/asessor/criteria/saved', 'AsessorPersonal\ApiController@getCriterionSaved');
Route::post('/asessor/criteria/reset-saved', 'AsessorPersonal\ApiController@resetSaved');

Route::post('/change-total/request-for-change', 'Admin\Api\ChangeTotalController@requestForChange');
//Route::post('/criteria/all', 'AsessorPersonal\ApiController@getAllCriteria');
