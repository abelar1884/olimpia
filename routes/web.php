<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/agree', 'Auth\RegisterController@agree')->name('agree');

/**
 * Registration
 */
/*
Route::get('/register', 'Auth\RegisterController@index')->name('register');
Route::post('/registered', 'Auth\RegisterController@create');
Route::get('/verify', 'Auth\RegisterController@verify')->name('verify');
*/
/**
 * Auth
 */

Route::get('/login', 'Auth\LoginController@showLogin')->name('login');
Route::post('/auth', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

/**
 *
 */
Route::get('/asessor/activate/{token}', 'Admin\AsessorController@activate')->name('asessor.activate');
Route::post('/asessor/activate/store/{key}', 'Admin\AsessorController@activateStore');
/*
Route::middleware(['auth', 'verified'])->group(function () {

    Route::get('/pa', 'PersonalController@index')->name('pa.index');
    Route::get('/pa/organization', 'PersonalController@org')->name('pa.org');
    Route::get('/pa/organization/update', 'PersonalController@orgUpdate')->name('pa.org.update');
    Route::post('/pa/organization/store', 'PersonalController@orgStore')->name('pa.org.store');

});
*/

Route::middleware(['auth'])->group(function () {

    Route::get('/admin', 'Admin\MainController@index')->name('admin.index');

    Route::get('/admin/reports', 'Admin\ReportController@index')->name('admin.report');
    Route::get('/admin/create/report', 'Admin\ReportController@create')->name('admin.report.create');
    Route::post('/admin/store/report', 'Admin\ReportController@store');
    Route::get('/admin/report/edit/{id}', 'Admin\ReportController@edit')->name('admin.report.edit');
    Route::post('/admin/report/update/{report}', 'Admin\ReportController@update');


    Route::get('/admin/asessors', 'Admin\AsessorController@index')->name('admin.asessor');
    Route::get('/admin/asessor/create', 'Admin\AsessorController@create')->name('admin.asessor.create');
    Route::post('/admin/asessor/store', 'Admin\AsessorController@store')->name('admin.asessor.store');
    Route::post('/admin/asessor/delete-link/{id}', 'Admin\AsessorController@deleteLink')->name('admin.asessor.deletelink');
    Route::get('/admin/asessor/single/{id}', 'Admin\AsessorController@single')->name('admin.asessor.single');


    Route::get('/admin/criterions', 'Admin\CriterionController@index')->name('admin.criterion');
    Route::post('/admin/criterion/store', 'Admin\CriterionController@store');
    Route::post('/admin/criterion/delete/{id}', 'Admin\CriterionController@delete')->name('admin.fira.delete');

    Route::resource('admin/change', 'Admin\ChangeTotalController');

    /*
    Route::get('/admin/users/', 'Admin\UserController@index')->name('admin.user');
    Route::get('/admin/user/{id}', 'Admin\UserController@single')->name('admin.user.single');

    Route::get('/admin/groups', 'Admin\GroupController@index')->name('admin.group');
    Route::get('/admin/group/{group}', 'Admin\GroupController@single')->name('admin.group.single');
    Route::post('/admin/group/add', 'Admin\GroupController@add');
    Route::post('/admin/group/update/{object}', 'Admin\GroupController@update');
    Route::get('/admin/group/delete/{object}', 'Admin\GroupController@delete')->name('admin.group.delete');
    */

});

Route::middleware(['auth'])->group(function () {

    Route::get('/asessor', 'AsessorPersonal\MainController@index')->name('asessor.index');
    Route::get('/asessor/reports', 'AsessorPersonal\MainController@reports')->name('asessor.reports');
    Route::get('/asessor/report/single/{id}', 'AsessorPersonal\MainController@reportSingle')->name('asessor.report.single');

    Route::get('/asessor/report/all-criterion/{report_id}', 'AsessorPersonal\MainController@allCriterion')->name('asessor.report.allcriterion');
    Route::get('/asessor/report/criterion/{report}/{criterion}', 'AsessorPersonal\MainController@criterion')->name('asessor.report.criterion');
    Route::post('/asessor/criteria/save', 'AsessorPersonal\MainController@saveCriterion');
    Route::post('/asessor/estimate', 'AsessorPersonal\MainController@estimate');

});
