<?php

use Illuminate\Database\Seeder;

class UserDataSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fields = \App\Models\UserData::fields();
        \App\Models\UserData::create($fields);
    }
}
