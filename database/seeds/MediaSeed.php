<?php

use Illuminate\Database\Seeder;

class MediaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i<20; $i++)
        {
            \App\Models\Media::create([
                'name' => str_random(10),
                'user_id' => rand(1,20),
                'nomination' => str_random(10),
                'rating' => str_random(10),
                'expert' => str_random(10),
                'storage' => str_random(10),
            ]);
        }
    }
}
