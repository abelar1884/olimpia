<?php

use Illuminate\Database\Seeder;
use App\Models\MiddleRating as Ass;

class MiddleRating extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //\Illuminate\Support\Facades\DB::statement('ALTER TABLE middle_ratings ADD COLUMN list_ratings VARCHAR(100)[][]');

        Ass::create([
            'asessor_id' => 1,
            'report_id' => 1,
            'list_ratings' => json_encode([
                ['ХУЙ', 'ЖОПА'],
                [1,1]
            ])
        ]);
    }
}
