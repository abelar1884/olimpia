<?php

use Illuminate\Database\Seeder;

class MiddleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i<10; $i++)
        {
            \App\Models\MiddleRating::create([
                'asessor_id' => rand(1,10),
                'report_id' => 3,
                'list_ratings' => '{"field_1":"1","field_2":"2"}',
                'final' => true
            ]);
        }
    }
}
