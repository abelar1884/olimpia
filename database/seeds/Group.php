<?php

use Illuminate\Database\Seeder;

class Group extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $block_name  = 'Блок_';
        $group_name  = 'Группа_';

        for ($i=0; $i<20; $i++)
        {
            \App\Models\Ratings\Block::create([
                'number' => $i,
                'name' => $block_name.$i
            ]);

            \App\Models\Ratings\CriterionGroup::create([
                'name' => $group_name.$i,
                'parent_id' => rand(1,20),
                'block_id' => rand(1,20)
            ]);
        }
    }
}
