<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $family = [
            'Смирнов',
            'Иванов',
            'Кузнецов',
            'Соколов',
            'Попов',
            'Лебедев',
            'Козлов',
            'Новиков',
            'Морозов',
            'Петров',
            'Волков',
            'Соловьёв',
            'Васильев',
            'Зайцев',
            'Павлов',
            'Семёнов',
            'Голубев',
            'Виноградов',
            'Богданов',
            'Воробьёв',
            'Фёдоров',
            'Михайлов',
            'Беляев',
            'Тарасов',
            'Белов'
        ];

        $twoName = [
            'Валентинович',
            'Валериевич',
            'Всеволодович',
            'Вячеславович',
            'Гаврилович',
            'Геннадиевич',
            'Георгиевич',
            'Давыдович',
            'Данилович',
            'Денисович',
            'Дмитриевич',
            'Евгеньевич',
            'Егорович',
            'Емельянович',
            'Ефимович',
            'Иванович',
            'Игоревич',
            'Ильич',
            'Иосифович',
        ];

        $name = [
            'Георгий',
            'Герман',
            'Глеб',
            'Гордей',
            'Григорий',
            'Давид',
            'Дамир',
            'Даниил',
            'Демид',
            'Демьян',
            'Денис',
            'Дмитрий',
            'Евгений',
            'Егор',
            'Елисей',
            'Захар',
            'Иван',
            'Игнат',
            'Игорь',
            'Илья',
            'Ильяс',
            'Камиль',
            'Карим',
            'Кирилл',
            'Клим',
        ];

        for ($i=0; $i<20; $i++)
        {
            $data = \App\Models\AsessorData::create([
                'phone' => '689769876'
            ]);

            \App\Models\User::create([
                'name'=> $family[rand(0,24)].' '.$name[rand(0,19)].' '.$twoName[rand(0,10)],
                'email' => str_random(10).'@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12341234'),
                'active' => true,
                'role' => 'asessor',
                'asessor_id' => $data->id
            ]);


        }


        \App\Models\User::create([
            'name'=> 'admin',
            'email' => 'admin@admin.com',
            'password' => \Illuminate\Support\Facades\Hash::make('12341234'),
            'active' => true,
            'role' => 'admin'
        ]);

        \App\Models\User::create([
            'name'=> 'Хомутецкий Эдуард Юрьевич',
            'email' => 'as@admin.com',
            'password' => \Illuminate\Support\Facades\Hash::make('12341234'),
            'active' => true,
            'role' => 'asessor'
        ]);


    }
}
