<?php

use Illuminate\Database\Seeder;

class ReportSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a = 1;

        for($i=0; $i<20; $i++)
        {
            $fiels = [
                'nomination_id' => 5,
                'uid' => $a,
                'full_name' => 'Полное наименование организации',
                'ur_address' => 'Юридический адрес',
                'fiz_address' => 'Фактический адрес',
                'region' => 2,
                'phone' => 'Телефон организации',
                'fax' => 'Факс организации',
                'email' => 'E-Mail организации',
                'header' => 'Руководитель организации',
                'position_header' => 'Должность руководителя',
                'subdivision' => 'Наименование подразделения, организующего проектную деятельность',
                'contact_name' => 'ФИО контактного лица',
                'contact_position' => 'Должность контактного лица',
                'contact_phone' => 'Телефон контактного лица',
                'contact_fax' => 'Факс контактного лица',
                'contact_email' => 'E-Mail контактного лица'];

            \App\Models\Report::create($fiels);

            $a++;
        }
    }
}
