<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiddleRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('middle_ratings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('asessor_id');
            $table->unsignedInteger('report_id');
            $table->unsignedInteger('criteria_id');
            $table->float('param_1');
            $table->float('param_2');
            $table->float('param_3');
            $table->float('param_4');
            $table->float('best_practice');
            $table->float('rated');
            $table->float('rating_as_1');
            $table->float('rating_as_2');
            $table->float('deviation');
            $table->float('corrected');
            $table->float('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('middle_ratings');
    }
}
