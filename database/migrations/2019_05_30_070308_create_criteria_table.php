<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCriteriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('criteria', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name');
            $table->text('desc');
            $table->unsignedBigInteger('max_value');
            $table->float('param_1');
            $table->float('param_2');
            $table->float('param_3');
            $table->float('param_4');
            $table->unsignedInteger('block')->nullable();
            $table->unsignedInteger('group')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criteria');
    }
}
