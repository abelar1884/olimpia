const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .js('resources/js/reports/edit.js', 'public/js/reports')
   .js('resources/js/reports/selectUploadDocs.js', 'public/js/reports')
   .js('resources/js/reports/delete.js', 'public/js/reports')
   .js('resources/js/reports/add-asessors.js', 'public/js/reports')
   .js('resources/js/asessors/delete.js', 'public/js/asessors')
   .js('resources/js/asessors_personal/index.js', 'public/js/asessors_personal')
   .js('resources/js/asessors_personal/ratingPick.js', 'public/js/asessors_personal')
   .js('resources/js/asessors_personal/report.js', 'public/js/asessors_personal')
   .sass('resources/sass/app.sass', 'public/css')
   .sass('resources/sass/asessor.sass', 'public/css');
