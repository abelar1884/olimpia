<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $activationLink = route('verify', [
            'id' => $this->user->id,
            'token' => $this->user->token,
        ]);

        return $this->subject('Подтверждение e-mail')
            ->view('mails.verifyemail')->with([
                'link' => $activationLink,
                'name' => $this->user->name
            ]);
    }
}
