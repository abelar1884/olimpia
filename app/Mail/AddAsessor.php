<?php

namespace App\Mail;

use App\Models\ActivateAsessor;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Hash;

class AddAsessor extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $token = str_random(15);

        ActivateAsessor::create([
            'email' => $this->email,
            'token' => $token,
            'activated' => false
        ]);

        $activationLink = route('asessor.activate', [
            'token' => $token
        ]);

        return $this->subject('Регистрация асессора')
            ->view('mails.addasessor')->with([
                'link' => $activationLink
            ]);
    }
}
