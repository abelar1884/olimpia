<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AsessorGroup extends Model
{
    protected $fillable = [
        'asessor_id',
        'group_id',
    ];
}
