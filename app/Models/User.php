<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Scout\Searchable;

class User extends Authenticatable
{
    use Notifiable;
    use Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'token',
        'userdata_id',
        'asessor_id',
        'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function medias()
    {
        return $this->hasMany(Media::class);
    }

    /**
     * Пользовательские данные
     */
    public function data()
    {
        return $this->belongsTo('App\Models\UserData','userdata_id');
    }

    /**
     * Данные асессоров
     */

    public function asessorGroup()
    {
        return $this->belongsToMany(Group::class,'asessor_groups','asessor_id','group_id');
    }

    public function asReports()
    {
        return $this->belongsToMany(Report::class,'report_asessors','asessor_id','report_id');
    }

    public function nominationAs()
    {
        return $this->belongsToMany(Nomination::class,'nomination_asessors','asessor_id','nomination_id');
    }

    public function dataAs()
    {
        return $this->belongsTo(AsessorData::class, 'asessor_id');
    }

    public function reportsAs()
    {
        return $this->belongsToMany(Report::class,'report_asessors','asessor_id','report_id');
    }

    /**
     * Настройки поиска
     */

    public function toSearchableArray()
    {
        return [
            'name' => $this->name,
        ];
    }
}
