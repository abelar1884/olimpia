<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Rating extends Model
{
    protected $fillable = [
        'asessor_id',
        'report_id',
        'total_value'
    ];

    public function report()
    {
        return $this->belongsTo(Report::class,'report_id');
    }

    public static function getRating($report_id, $asessor_id)
    {
        $totals = DB::select('SELECT total FROM middle_ratings WHERE report_id=? AND asessor_id=?', [$report_id, $asessor_id]);

        $result = 0.0;

        foreach ($totals as  $total)
        {
            $result += $total->total;
        }

        return $result;
    }

    public static function getMaxValue()
    {
        $totals = DB::select('SELECT max_value FROM criteria');

        $result = 0.0;

        foreach ($totals as  $total)
        {
            $result += $total->max_value;
        }

        return $result;
    }

    public static function getOtherRatings()
    {
        return 'Нет оценок других асессоров';
    }
}
