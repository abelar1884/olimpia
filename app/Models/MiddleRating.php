<?php

namespace App\Models;

use App\Models\Ratings\Criterion;
use Illuminate\Database\Eloquent\Model;

class MiddleRating extends Model
{
    protected $fillable = [
        'asessor_id',
        'report_id',
        'criteria_id',
        'param_1',
        'param_2',
        'param_3',
        'param_4',
        'best_practice',
        'rated',
        'rating_as_1',
        'rating_as_2',
        'deviation',
        'corrected',
        'total',
    ];

    public function asessor()
    {
        return $this->belongsTo(User::class, 'asessor_id');
    }

    public function report()
    {
        return $this->belongsTo(Report::class, 'report_id');
    }

    public function criterion()
    {
        return $this->belongsTo(Criterion::class, 'criteria_id');
    }
}
