<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
        'name',
        'header'
    ];

    public function headers()
    {
        return $this->belongsTo(User::class,'header');
    }

    public function asessors()
    {
        return $this->belongsToMany(User::class,'asessor_groups','group_id','asessor_id');
    }
}
