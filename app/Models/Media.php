<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = [
        'number',
        'name',
        'mediable_id',
        'mediable_type',
        'storage',
        'extension'
    ];

    public function mediable()
    {
        return $this->morphTo();
    }

}
