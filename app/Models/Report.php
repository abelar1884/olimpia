<?php

namespace App\Models;

use App\Models\Ratings\ChangeTotal;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
        'nomination_id',
        'full_name',
        'ur_address',
        'fiz_address',
        'region',
        'phone',
        'fax',
        'email',
        'header',
        'position_header',
        'subdivision',
        'contact_name',
        'contact_position',
        'contact_phone',
        'contact_fax',
        'contact_email',
        'drive_link',
        'count_files',
        'uid'
    ];

    public static function fields ()
    {
        return [
            //'nomination_id' => 12,
            //'full_name' => 'Полное наименование организации *',
            'ur_address' => 'Юридический адрес',
            'fiz_address' => 'Фактический адрес',
            //'region' => 'Регион',
            //'phone' => 'Телефон организации',
            //'fax' => 'Факс организации',
            //'email' => 'E-Mail организации',
            //'header' => 'Руководитель организации',
            //'position_header' => 'Должность руководителя',
            //'subdivision' => 'Наименование подразделения, организующего проектную деятельность',
            'contact_name' => 'ФИО контактного лица',
            'contact_position' => 'Должность контактного лица',
            'contact_phone' => 'Телефон контактного лица',
            //'contact_fax' => 'Факс контактного лица',
            'contact_email' => 'E-Mail контактного лица',
            //'drive_link' => 'Ссылка на хранилище'
        ];
    }

    public function nomination()
    {
        return $this->belongsTo(Nomination::class);
    }

    public function medias()
    {
        return $this->morphMany(Media::class,'mediable')->orderBy('number','asc');
    }

    public function asessors()
    {
        return $this->belongsToMany(User::class,'report_asessors','report_id','asessor_id');
    }

    public function regions()
    {
        return $this->belongsTo(Region::class, 'region');
    }

    public function middleRating()
    {
        return $this->hasMany(MiddleRating::class, 'report_id');
    }

    public function rating()
    {
        return $this->hasOne(Rating::class, 'report_id');
    }

    public function changeTotal()
    {
        return $this->hasOne(ChangeTotal::class);
    }

    /**
     * Настройки поиска
     */

    public function toSearchableArray()
    {
        return [
            'full_name' => $this->full_name,
            'uid' => $this->uid
        ];
    }
}
