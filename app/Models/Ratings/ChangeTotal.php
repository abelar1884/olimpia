<?php

namespace App\Models\Ratings;

use App\Models\Report;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class ChangeTotal extends Model
{
    protected $fillable = [
        'report_id',
        'asessor_id',
        'cause'
    ];

    public function report()
    {
        return $this->belongsTo(Report::class,'report_id');
    }

    public function asessor()
    {
        return $this->belongsTo(User::class,'asessor_id');
    }
}
