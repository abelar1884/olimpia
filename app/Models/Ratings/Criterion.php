<?php

namespace App\Models\Ratings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Criterion extends Model
{
    protected $fillable = [
        'name',
        'desc',
        'max_value',
        'param_1',
        'param_2',
        'param_3',
        'param_4',
        'block',
        'group'
    ];

    public static function getParam()
    {
        return [
            'param_1' => 'Нормативно-регламентное  обеспечение',
            'param_2' => 'Информационная система управления',
            'param_3' => 'Артефакты',
            'param_4' => 'Качество процесса',
        ];
    }

    public static function estimate($asessor_id, $report_id, $criterion_id)
    {
        $result = DB::select('SELECT * FROM middle_ratings WHERE asessor_id=? AND report_id=? AND criteria_id=?', [$asessor_id, $report_id, $criterion_id]);

        return array_shift($result);
    }
}
