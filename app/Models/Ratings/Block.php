<?php

namespace App\Models\Ratings;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $fillable = [
        'number',
        'name'
    ];
}
