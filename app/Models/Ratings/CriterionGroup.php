<?php

namespace App\Models\Ratings;

use Illuminate\Database\Eloquent\Model;

class CriterionGroup extends Model
{
    protected $table = 'criterion_groups';

    protected $fillable = [
        'name',
        'parent_id',
        'block_id'
    ];
}
