<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    protected $fillable = [
        'full_name',
        'ur_address',
        'fiz_address',
        'phone',
        'fax',
        'email',
        'header',
        'position_header',
        'subdivision',
        'contact_name',
        'contact_position',
        'contact_phone',
        'contact_fax',
        'contact_email'
    ];

    public static function fields ()
    {
        return [
            'full_name' => 'Полное наименование организации',
            'ur_address' => 'Юридический адрес',
            'fiz_address' => 'Фактический адрес',
            'phone' => 'Телефон организации',
            'fax' => 'Факс организации',
            'email' => 'E-Mail организации',
            'header' => 'Руководитель организации',
            'position_header' => 'Должность руководителя',
            'subdivision' => 'Наименование подразделения, организующего проектную деятельность',
            'contact_name' => 'ФИО контактного лица',
            'contact_position' => 'Должность контактного лица',
            'contact_phone' => 'Телефон контактного лица',
            'contact_fax' => 'Факс контактного лица',
            'contact_email' => 'E-Mail контактного лица'
        ];
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }
}
