<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivateAsessor extends Model
{
    protected $fillable = [
        'email',
        'token',
        'activated'
    ];
}
