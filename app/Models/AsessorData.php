<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AsessorData extends Model
{
    protected $fillable = [
        'phone'
    ];
}
