<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NominationAsessor extends Model
{
    protected $fillable = [
        'asessor_id',
        'nomination_id'
    ];
}
