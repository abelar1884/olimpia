<?php
/**
 * Created by PhpStorm.
 * User: Эдуард
 * Date: 17.12.2018
 * Time: 11:00
 */

namespace App\Modules;

use App\Models\User;

class Active
{
    public static function verification($id, $token)
    {
        $user = User::find($id);
        if (!$user)
        {
            return 'Такой пользователь не зарегистрирован';
        }

        if ($user->token != $token)
        {
            return 'Код активации не действителен';
        }

        $user->token = '';
        $user->active = true;
        $user->save();

        return 'E-mail подтвержден. Вы можете войти в <a href="'.route('login').'">личный кабинет</a>';
    }
}