<?php

namespace App\Modules\Traits;

use App\Models\Ratings\Criterion;
use Illuminate\Http\Request;

trait CalculateRated
{


    public function calculateDeviation(Request $request, $id)
    {
        $result = abs($request->input('rating_as_2')[$id]-$request->input('rating_as_1')[$id]);

        return $result;
    }

    public function calculateTotal(Request $request, $max_value, $id)
    {
        if ($request->input('corrected')[$id])
        {
            return $request->input('corrected')[$id]*$max_value;
        }

    }
}