<?php
/**
 * Created by PhpStorm.
 * User: Эдуард
 * Date: 07.06.2019
 * Time: 12:23
 */

namespace App\Modules\Classes;

use App\Models\MiddleRating;
use Illuminate\Http\Request;
use App\Models\Ratings\Criterion;
use Illuminate\Support\Facades\DB;

class Estimate
{

    private $request;
    private $criterion;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->criterion = Criterion::all();
    }



    /****************************************************************************************************************
     *
     *  Расчетное значение
     *
     ****************************************************************************************************************/

    private function calculateRated($id)
    {
        $addend = [
            1 => $this->request->input('param_1')[$id]*$this->criterion->param_1,
            2 => $this->request->input('param_2')[$id]*$this->criterion->param_2,
            3 => $this->request->input('param_3')[$id]*$this->criterion->param_3,
            4 => $this->request->input('param_4')[$id]*$this->criterion->param_4,
        ];

        $result = (($addend[1]+$addend[2]+$addend[3]+$addend[4])/3)*0.8+$this->request->input('best_practice')[$id];

        return $result;
    }



    /****************************************************************************************************************
     *
     *  Расчетное значение
     *
     ****************************************************************************************************************/

    private function calculateTotal($id)
    {
        if ($this->request->input('corrected')[$id])
        {
            return $this->request->input('corrected')[$id]*$this->criterion->max_value;
        }
        return ($this->request->input('rating_as_1')[$id]+$this->request->input('rating_as_2')[$id])/2*$this->criterion->max_value;

    }



    /****************************************************************************************************************
     *
     *  Расчетное значение
     *
     ****************************************************************************************************************/

    public function getResult($id)
    {
        return $this->calculateTotal($id);
    }
}