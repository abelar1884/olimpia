<?php
/**
 * Created by PhpStorm.
 * User: Эдуард
 * Date: 13.05.2019
 * Time: 11:57
 */

namespace App\Modules\Classes;


class FileExtension
{
    public static function incon($extension)
    {
        switch ($extension) {
            case 'pdf':
                return asset('/images/icons/pdf.png');
            case 'doc':
                return asset('/images/icons/doc.png');
            case 'docx':
                return asset('/images/icons/docx.png');
            case 'xls':
                return asset('/images/icons/xls.png');
            case 'xlsx':
                return asset('/images/icons/xlsx.png');
            case 'zip':
                return asset('/images/icons/zip.png');
            case 'rar':
                return asset('/images/icons/rar.png');
        }
    }
}