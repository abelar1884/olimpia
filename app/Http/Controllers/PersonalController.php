<?php

namespace App\Http\Controllers;

use App\Models\UserData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Media;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PersonalController extends Controller
{
    protected $path = 'personal_area';

    public function index()
    {
        return view($this->path.'.index',['title' => 'Личный кабинет']);
    }

    public function org()
    {
        return view($this->path.'.org.index',[
            'title' => 'Сведения о организации',
            'fields' => UserData::fields(),
            'data' => Auth::user()->data,
            'files' => Auth::user()->medias
        ]);
    }

    public function orgUpdate()
    {
        $values = Auth::user()->data ? Auth::user()->data : false;
        return view($this->path.'.org.update',[
            'title' => 'Внести сведения о организации',
            'fields' => UserData::fields(),
            'values' => $values,
            'files' => Auth::user()->medias
        ]);
    }

    public function orgStore(Request $request)
    {
        $user = $request->user();

        if ($user->data)
        {
            $data = $user->data;
            $data->fill($request->all());
            $data->save();


            if ($request->input('delete_files'))
            {
                foreach ($request->input('delete_files') as $file)
                {
                    Media::destroy(explode('|',$file)[0]);
                    Storage::disk('public')->delete(explode('|',$file)[1]);
                }
            }

            if ($request->file('files'))
            {
                foreach ($request->file('files') as $file) {
                    Media::create([
                        'name' => $file->getClientOriginalName(),
                        'user_id' => $user->id,
                        'storage' => $file->store('user_upload', 'public')
                    ]);
                }
            }

            return redirect()->route('pa.org')->with('success', 'Данные успкшно сохранены');
        }
        else {

            $data_id = UserData::create($request->all());

            $user->userdata_id = $data_id->id;
            $user->save();

            if ($request->file('files')) {
                foreach ($request->file('files') as $file) {
                    Media::create([
                        'name' => $file->getClientOriginalName(),
                        'user_id' => $user->id,
                        'storage' => $file->store('user_upload', 'public')
                    ]);
                }
            }

            return redirect()->route('pa.org')->with('success', 'Данные успкшно сохранены');
        }

    }
}
