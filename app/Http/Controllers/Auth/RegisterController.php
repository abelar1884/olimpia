<?php

namespace App\Http\Controllers\Auth;

use App\Mail\VerifyEmail;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Modules\Active;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    public function index()
    {
        return view('auth.register',['title'=>'Регистрация']);
    }

    public function agree()
    {
        return view('auth.agree',['title'=>'Политика конфиденциальности']);
    }

    public function verify()
    {
        if (isset($_GET['id']) and isset($_GET['token']))
        {
            $result = Active::verification($_GET['id'], $_GET['token']);
            return view('auth.verify',['title'=>'Подтверждение e-mail','active'=>$result]);
        }
        else{
            return redirect('/login')->with('error','Ссылка не действительна');
        }
    }

    public function create(Request $request)
    {
        $message = [
            'name.required' => 'Имя обязательно для заполнения',
            'email.required' => 'E-Mail обязательно для заполнения',
            'email.email' => 'Не корректно введен e-mail',
            'email.unique' => 'Пользователь с данным е-mail уже зарегистрирован',
            'password.required' => 'Пароль обязательно для заполнения',
            'password.confirmed' => 'Проли не совпадают',
            'password.min' => 'Пароль должен быть не короче 6 символов',
            'agree.required' => 'Ознакомтесь с политикой конфиденциальности'
        ];

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:6',
            'agree' => 'required'
        ], $message);

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'token' => Hash::make($request->input('email')),
            'role' => 'user'
        ]);

        Mail::to($request->input('email'))->send(new VerifyEmail($user));

        /*
        foreach ($request->file('files') as $file)
        {
            Media::create([
                'name' => $file->getClientOriginalName(),
                'user_id' => $user->id,
                'storage' => $file->store('user_upload','public')
            ]);
        }
        */

        return redirect()->back()->with('success', 'Вы успешно зарегистрировались');
    }

}
