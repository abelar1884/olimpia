<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLogin ()
    {
        return view('auth.login',['title' => 'Авторизация']);
    }

    public function login(Request $request)
    {
        $user = User::where('email',$request->input('email'))->first();

        if (!$user)
        {
            return redirect()->route('login')->with('errors', 'Неверно указан e-mail или пароль');
        }

        if (!$user->active)
        {
            return redirect()->route('login')->with('errors','E-Mail не подтвержден');
        }

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')], true)) {

            if (Auth::user()->role == 'admin')
            {
                return redirect('/admin/reports');
            }
            elseif (Auth::user()->role == 'asessor')
            {
                return redirect()->route('asessor.index');
            }
            return redirect()->route('pa.index');
        }


        return redirect()->route('login')->with('errors', 'Неверно указан e-mail или пароль');
    }


}
