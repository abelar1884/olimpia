<?php

namespace App\Http\Controllers\Admin;

use App\Models\AsessorGroup;
use App\Models\Group;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    protected $path = 'admin_area.groups.';

    public function index()
    {
        return view($this->path.'index', [
            'title' => 'Все группы',
            'groups' => Group::orderBy('id', 'desc')->paginate(20)
        ]);
    }

    public function add(Request $request)
    {
        $message = [
            'name.required' => 'Необходимо ввести название группы',
            'name.unique' => 'Группа с таким именем уже создана'
        ];
        $request->validate([
            'name' => 'required|unique:groups,name'
        ], $message);

        Group::create($request->all());

        return redirect()->back()->with('success', 'Группа добавлена');
    }

    public function single(Group $group)
    {
        return view($this->path.'single',[
            'title' => 'Группа | '.$group->name,
            'group' => $group,
            'asessors' => User::where('role', 'asessor')->orderBy('id', 'asc')->get(),
            'thisAsessors' => $group->asessors
        ]);
    }

    public function update(Request $request, Group $object)
    {
        $unique = $request->input('name') != $object->name? '|unique:groups,name': '';

        $message = [
            'name.required' => 'Необходимо ввести название группы',
            'name.unique' => 'Группа с таким именем уже создана'
        ];
        $request->validate([
            'name' => 'required'.$unique
        ], $message);

        $object->name = $request->input('name');
        $object->header = $request->input('header');
        $object->save();

        $object->asessors()->detach();
        if ($request->input('asessors'))
        {
            foreach ($request->input('asessors') as $item) {
                AsessorGroup::create([
                    'asessor_id' => $item,
                    'group_id' => $object->id
                ]);
            }
        }

        return redirect()->back()->with('success', 'Данные сохранены');
    }

    public function delete(Group $object)
    {
        $object->asessors()->detach();
        $object->delete();
        return redirect()->back()->with('success', 'Группа удалена');
    }
}
