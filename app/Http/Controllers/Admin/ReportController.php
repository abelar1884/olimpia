<?php

namespace App\Http\Controllers\Admin;

use App\Models\Nomination;
use App\Models\Region;
use App\Models\Report;
use App\Models\ReportAsessor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Media;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ReportController extends Controller
{
    protected $path = 'admin_area.reports.';

    public function index()
    {
        return view($this->path.'index', [
            'title' => 'Загруженные отчеты',
            'reports' => Report::orderBy('id', 'asc')->paginate(20)
        ]);
    }

    public function create()
    {
        $nomination = [
            'Основные номинации' => Nomination::select('id','name')->where('type', 1)->get(),
            'Специальные номинации' => Nomination::select('id','name')->where('type', 2)->get(),
        ];

        return view($this->path.'create', [
            'title' => 'Добавить отчет',
            'regions' => Region::orderBy('id', 'asc')->get(),
            'fields' => Report::fields(),
            'nominations' => $nomination,
            'asessors' => User::where('role', 'asessor')->orderBy('name', 'asc')->get()
        ]);
    }




/************************************************************************************************************************************************
 *
 * Функция добавления отчета
 *
 *************************************************************************************************************************************************/

    public function store(Request $request)
    {
        /**************************************
         * Валидация данных
         **************************************/

        $massage = [
            'nomination_id.numeric' => 'Выберите номинацию',
            'region.numeric' => 'Выберите регион',
            'full_name.required' => 'Введите наменование организации',
            'uid.required' => 'Укажите номер отчета',
            'uid.unique' => 'Отчет с таким номером уже существует',
            'uid.numeric' => 'Номер должен быть целым числов'
        ];

        $request->validate([
            'nomination_id' => 'numeric',
            'region' => 'numeric',
            'full_name' => 'required',
            'files.*' => 'mimes:pdf,doc,docx,xls,xlsx,zip,rar',
            'uid' => 'required|unique:reports,uid|numeric'
        ], $massage);



        /***************************************
         * Создание отчета
         **************************************/

        $report = Report::create($request->all());



        /***************************************
         * Добавление асессоров
         **************************************/

        if ($request->input('asessors'))
        {
            $asessors = explode(',', $request->input('asessors'));

            foreach ($asessors as $asessor)
            {
                ReportAsessor::create([
                    'report_id' => $report->id,
                    'asessor_id' => $asessor
                ]);
            }
        }



        /***************************************
         * Добавление файлов
         **************************************/

        $number = 1;
        if ($request->hasFile('files'))
        {
            foreach ($request->file('files') as $file) {

                $fileInfo = pathinfo($file->getClientOriginalName());
                Media::create([
                    'number' => $number,
                    'name' => $fileInfo['filename'],
                    'mediable_id' => $report->id,
                    'mediable_type' => Report::class,
                    'storage' => $file->store('reports/'.$report->id, 'public'),
                    'extension' => $fileInfo['extension']
                ]);

                $number++;
            }
        }

        $report->count_files = $number;
        $report->save();

        return redirect()->route('admin.report')->with('success', 'Отчет добавлен');

    }

    public function edit(Report $id)
    {
        $nomination = [
            'Основные номинации' => Nomination::select('id','name')->where('type', 1)->get(),
            'Специальные номинации' => Nomination::select('id','name')->where('type', 2)->get(),
        ];

        return view($this->path.'edit', [
            'title' => 'Редактировать отчет',
            'regions' => Region::orderBy('id', 'asc')->get(),
            'fields' => Report::fields(),
            'object' => $id,
            'nominations' => $nomination,
        ]);
    }




/************************************************************************************************************************************************
 *
 * Функция изменеия отчета
 *
 *************************************************************************************************************************************************/

    public function update(Request $request, Report $report)
    {
        /**
         * Вадиация данныйх
         */

        $uidUnique = $request->input('uid') != $report->uid? '|unique:reports,uid': '';

        $massage = [
            'nomination_id.numeric' => 'Выберите номинацию',
            'full_name.required' => 'Введите наменование организации',
            'uid.required' => 'Укажите номер отчета',
            'uid.unique' => 'Отчет с таким номером уже существует',
        ];

        $request->validate([
            'nomination_id' => 'numeric',
            'full_name' => 'required',
            'files.*' => 'mimes:pdf,doc,docx,xls,xlsx,zip,rar',
            'uid' => 'required'.$uidUnique
        ], $massage);



        /**
         * Обновление данных Report
         */

        $report->fill($request->all())->update();




        /***************************************
         * Добавление асессоров
         **************************************/
        if ($request->input('asessors'))
        {
            $asessors = explode(',', $request->input('asessors'));

            foreach ($asessors as $asessor)
            {
                $isAsessor = DB::select('SELECT id FROM report_asessors WHERE asessor_id=? AND report_id=?', [
                    $asessor,
                    $report->id
                ]);

                if (!array_shift($isAsessor))
                {
                    ReportAsessor::create([
                        'report_id' => $report->id,
                        'asessor_id' => $asessor
                    ]);
                }

            }
        }



        /**
         * Проверяем что бы не загрузили больше 10 файлов
         */

        $newFiles = $request->file('files')? count($request->file('files')): 0;
        if($report->medias->count() + $newFiles > 10)
        {
            return redirect()->back()->with('error', 'Максимум 10 документов может содержаться в одном отчете');
        }



        /**
         * Добавление новых файлов
         */

        if ($request->hasFile('files'))
        {
            $number = $report->count_files? $report->count_files: 1;

            foreach ($request->file('files') as $file) {

                $fileInfo = pathinfo($file->getClientOriginalName());

                Media::create([
                    'name' => $fileInfo['filename'],
                    'number' => $number,
                    'mediable_id' => $report->id,
                    'mediable_type' => Report::class,
                    'storage' => $file->store('reports/'.$report->id, 'public'),
                    'extension' => $fileInfo['extension']
                ]);

                $number++;
            }

            $report->count_files = $number;
            $report->save();
        }



        /**
         * Изменение имен файлов
         */

        if ($request->input('file_name_ids'))
        {
            $medias = Media::find($request->input('file_name_ids'));

            foreach ($medias as $media)
            {
                $media->name = $request->input('file_name_'.$media->id);
                $media->save();
            }

        }

        return redirect()->back()->with('success', 'Данные сохранены');

    }

}
