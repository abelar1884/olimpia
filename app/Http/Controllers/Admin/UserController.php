<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use  App\Models\UserData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    protected $path = 'admin_area.users.';

    public function index()
    {
        return view($this->path.'index', [
            'title' => 'Все пользователи',
            'users' => User::orderBy('id', 'asc')->where('role', 'user')->paginate(20)
        ]);
    }

    public function single(User $id)
    {
        return view($this->path.'single', [
            'title' => 'Подробная информация пользователя',
            'user' => $id,
            'fields' => UserData::fields(),
            'files' => $id->medias,
        ]);
    }
}
