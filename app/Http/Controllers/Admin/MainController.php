<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\UserData;
use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('admin_area.index',[
            'title' => 'Админ панель',
        ]);
    }

}
