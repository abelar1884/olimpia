<?php

namespace App\Http\Controllers\Admin;

use App\Models\Ratings\Criterion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CriterionController extends Controller
{
    protected $path = 'admin_area.criterions.';

    public function index()
    {
        return view($this->path.'index', [
            'title' => 'Критерии оценки',
            'fields' => Criterion::all(),
            'params' => Criterion::getParam()
        ]);
    }

    public function store(Request $request)
    {
        $message = [
            'name.required' => 'Введите название',
            'email.unique' => 'Такое поле уже создано',
        ];

        $request->validate([
            'name' => 'required|unique:criteria,name',
            'max_value' => 'required|numeric',
            'param_1' => 'required|numeric|min:0|max:1',
            'param_2' => 'required|numeric|min:0|max:1',
            'param_3' => 'required|numeric|min:0|max:1',
            'param_4' => 'required|numeric|min:0|max:1',
        ], $message);

        Criterion::create($request->all());

        return redirect()->back()->with('success', 'Поле добавлено');
    }

    public function delete(Criterion $id)
    {
        $id->delete();
        return redirect()->back()->with('success', 'Данные удалены');
    }
}
