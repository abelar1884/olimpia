<?php

namespace App\Http\Controllers\Admin;

use App\Mail\AddAsessor;
use App\Mail\RegisterAsessor;
use App\Models\ActivateAsessor;
use App\Models\AsessorData;
use App\Models\NominationAsessor;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Models\Nomination;

class AsessorController extends Controller
{
    protected $path = 'admin_area.asessors.';

    public function index()
    {
        return view($this->path.'index', [
            'title' => 'Ассесоры',
            'asessors' => User::where('role', 'asessor')->orderBy('id', 'asc')->paginate(10)
        ]);
    }

    public function getList()
    {
        return response()->json(User::where('role', 'asessor')->orderBy('id', 'asc')->get());
    }

    public function create()
    {
        return view($this->path.'create', [
            'title' => 'Добавить нового асессора',
            'links' => ActivateAsessor::orderBy('id', 'asc')->paginate(10)
        ]);
    }

    public function store(Request $request)
    {
        $message = [
            'email.required' => 'E-Mail обязательно для заполнения',
            'email.email' => 'Не корректно введен e-mail',
            'email.unique' => 'Асессор с данным е-mail зарегистрирован или ссылка сгенерированна',
        ];

        $request->validate([
            'email' => 'required|email|unique:users,email|unique:activate_asessors,email'
        ], $message);

        Mail::to($request->input('email'))->send(new AddAsessor($request->input('email')));

        return redirect()->back()->with('success', 'Ссылка отправлена');
    }

    public function deleteLink(ActivateAsessor $id)
    {
        $id->delete();
        return redirect()->back()->with('success', 'Ссылка удалена');
    }

    public function single(User $id)
    {
        return view($this->path.'single', [
            'title' => 'Подробная информация',
            'object' => $id,

        ]);
    }




/****************************************************************************************************************
*
* Страница ввода данных асессора
*
****************************************************************************************************************/


    public function activate($token)
    {
        $nomination = [
            'Основные номинации' => Nomination::select('id','name')->where('type', 1)->get(),
            'Специальные номинации' => Nomination::select('id','name')->where('type', 2)->get(),
        ];

        $key = ActivateAsessor::where('token', $token)->first()? ActivateAsessor::where('token', $token)->first(): false;
        return view($this->path.'.activate', [
            'title' => 'Активация личного кабинета асессора',
            'key' => $key,
            'nominations' => $nomination,
        ]);
    }



/****************************************************************************************************************
 *
 * Подтверждение асессора
 *
 ****************************************************************************************************************/
    public function activateStore(Request $request, ActivateAsessor $key)
    {
        $message = [
            'name.required' => 'Имя обязательно для заполнения',
            'email.required' => 'E-Mail обязательно для заполнения',
            'email.email' => 'Не корректно введен e-mail',
            'email.unique' => 'Пользователь с данным е-mail уже зарегистрирован',
            'password.required' => 'Пароль обязательно для заполнения',
            'password.confirmed' => 'Проли не совпадают',
            'password.min' => 'Пароль должен быть не короче 6 символов',
            'agree.required' => 'Ознакомтесь с политикой конфиденциальности',
            'phone.required' => 'Укажите номер телефона'
        ];

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:6',
            'phone' => 'required',
            'agree' => 'required'
        ], $message);

        $data = AsessorData::create([
            'phone' => $request->input('phone')
        ]);

        $asessor = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'token' => Hash::make($request->input('email')),
            'role' => 'asessor',
            'asessor_id' => $data->id,
            'active' => true
        ]);

        foreach ($request->input('nominations') as $nomination)
        {
            NominationAsessor::create([
                'asessor_id' => $asessor->id,
                'nomination_id' => $nomination
            ]);
        }

        Mail::to($asessor->email)->send(new RegisterAsessor($asessor, $request->input('password')));

        $key->activated = true;
        $key->token = '';
        $key->save();

        return redirect()->route('login')->with('success', 'Личный кабинет активирован');
    }

}
