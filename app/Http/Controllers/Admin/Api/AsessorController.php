<?php

namespace App\Http\Controllers\Admin\Api;

use App\Models\ReportAsessor;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AsessorController extends Controller
{

    public function getAll()
    {
        return response(json_encode(User::orderBy('name', 'asc')->where('role', 'asessor')->get(['id', 'name'])));
    }


    public function getNotIncludeReport(Request $request)
    {

        $asessors= DB::select("SELECT id, name FROM users where role='asessor' and NOT EXISTS 
                                      (SELECT * FROM report_asessors WHERE users.id = report_asessors.asessor_id 
                                      AND report_asessors.report_id = ?) ORDER BY name ASC ", [$request->input('report_id')]);
        return response(json_encode($asessors));

    }


    public function delete(Request $request)
    {
        $asessor = User::find($request->input('asessor'));
        DB::select('delete from asessor_data WHERE id=?', [$asessor->dataAs->id]);
        DB::select('delete from nomination_asessors WHERE asessor_id=?', [$asessor->id]);
        $asessor->delete();
    }


    public function search(Request $request)
    {
        if (!$request->input('name'))
        {
            return response(json_encode(User::where('role', 'asessor')->orderBy('name', 'asc')->get(['id', 'name'])));
        }
        return response(json_encode(User::search(str_ireplace(' ', '&',$request->input('name')))->where('role', 'asessor')->get()));
    }


    public function searchExclude(Request $request)
    {
        if (!$request->input('name'))
        {
            $asessors= DB::select("SELECT id, name FROM users where role='asessor' and NOT EXISTS 
                                      (SELECT * FROM report_asessors WHERE users.id = report_asessors.asessor_id 
                                      AND report_asessors.report_id = ?) ORDER BY name ASC ", [$request->input('report_id')]);
            return response(json_encode($asessors));
        }
        else
        {

            $addedAs = DB::select("select id, name,
  ts_rank(searchable,tsquery) AS rank, COUNT(*) OVER () AS total_count
from users cross join to_tsquery(COALESCE('english', get_current_ts_config()), ?) AS tsquery where searchable @@ tsquery
and role='asessor'
AND NOT EXISTS (SELECT * FROM report_asessors WHERE users.id = report_asessors.asessor_id
and report_id = ?)order by rank desc, id asc", [str_ireplace(' ', '&',$request->input('name')), $request->input('report_id')]);
            return response(json_encode($addedAs));
        }

    }


    public function getNameById(Request $request)
    {
        return response( json_encode( User::whereIn('id', $request->input('ids'))->get(['name']) ) );
    }
}
