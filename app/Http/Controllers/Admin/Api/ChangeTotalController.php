<?php

namespace App\Http\Controllers\Admin\Api;

use App\Models\Ratings\ChangeTotal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ChangeTotalController extends Controller
{
    public function requestForChange(Request $request)
    {
        /*
        $isRequest = DB::select('SELECT id FROM change_totals WHERE report_id=? AND asessor_id=?', [
            $request->input('report_id'),
            $request->input('asessor_id')
        ]);

        if (array_shift($isRequest))
        {
            //
        }
        */

        ChangeTotal::create($request->all());

        return response(json_encode(['resp' => true]));
    }
}
