<?php

namespace App\Http\Controllers\Admin\Api;

use App\Models\Nomination;
use App\Models\Region;
use App\Models\Report;
use App\Models\ReportAsessor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Media;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ReportController extends Controller
{

    /***************************************************************************************************************************************************
     *
     * API
     *
     ***************************************************************************************************************************************************/
    public function deleteFiles(Request $request)
    {
        $media = Media::find($request->input('doc_id'));

        Storage::disk('public')->delete($media->storage);
        $media->delete();

        return response(json_encode('true'));
    }

    public function addedAsessors(Request $request)
    {
        $record = Report::find($request->input('report_id'));
        $asessors = $record->asessors;
        return response(json_encode($asessors));
    }

    public function deleteAsessor(Request $request)
    {
        DB::select('DELETE from report_asessors WHERE report_id=? and asessor_id=?',[
            $request->input('report'),
            $request->input('asessor')
        ]);
    }

    public function delete(Request $request)
    {
        $report = Report::find($request->input('report_id'));
        //return var_dump($report);
        if ($report->asessors)
        {
            DB::select('delete from report_asessors WHERE report_id=?', [$report->id]);
        }
        if ($report->medias)
        {
            foreach ($report->medias as $media)
            {
                Storage::disk('public')->delete($media->storage);
                $media->delete();
            }
        }

        $report->delete();

        return response(json_encode('true'));
    }
}
