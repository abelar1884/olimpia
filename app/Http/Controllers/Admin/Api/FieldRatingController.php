<?php

namespace App\Http\Controllers\Admin\Api;

use App\Models\FieldRating;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FieldRatingController extends Controller
{
    public function getAll()
    {
        return response(json_encode(FieldRating::all(['id','name'])));
    }
}
