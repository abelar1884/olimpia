<?php

namespace App\Http\Controllers\AsessorPersonal;

use App\Models\MiddleRating;
use App\Models\Ratings\Criterion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    /****************************************************************************************************************
     *
     * Получение сохраненного рейтинга
     *
     ****************************************************************************************************************/

    public function getCriterionSaved(Request $request)
    {
        $saved = Criterion::estimate($request->input('user_id'), $request->input('report_id'), $request->input('criterion_id'));

        if ($saved)
        {
            return response(json_encode($saved));
        }
        return response(json_encode(['resp' => false]));
    }



    /****************************************************************************************************************
     *
     * Получение одного критерия
     *
     ****************************************************************************************************************/

    public function getCriterion(Request $request)
    {
        $result = DB::select('SELECT * FROM criteria WHERE id=?', [$request->input('id')]);

        return response(json_encode($result[0]));
    }



    /****************************************************************************************************************
     *
     * Получение всех критериев
     *
     ****************************************************************************************************************/

    public function getAllCriteria()
    {
        return response(json_encode(Criterion::all()));
    }



    /****************************************************************************************************************
     *
     * Сохраниение временного рейтинга
     *
     ****************************************************************************************************************/

    public function resetSaved(Request $request)
    {
        DB::select('DELETE FROM middle_ratings WHERE id=?', [$request->input('id')]);
    }



}
