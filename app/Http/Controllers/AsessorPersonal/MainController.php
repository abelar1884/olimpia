<?php

namespace App\Http\Controllers\AsessorPersonal;

use App\Models\MiddleRating;
use App\Models\Rating;
use App\Models\Ratings\Criterion;
use App\Models\Report;
use App\Modules\Classes\Estimate;
use App\Modules\Traits\CalculateRated;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{

    use CalculateRated;

    protected $path = 'asessors_area.';

    /****************************************************************************************************************
     *
     * Главная страница личного кабинета асессора
     *
     ****************************************************************************************************************/

    public function index()
    {
        return view($this->path.'index', [
            'title' => 'Личный кабинет асессора',
            'user' => Auth::user()
        ]);
    }



    /****************************************************************************************************************
     *
     * Старница отчетов асессора
     *
     ****************************************************************************************************************/

    public function reports()
    {
        return view($this->path.'reports', [
            'title' => 'Отчеты закрепленные за вами',
            'user' => Auth::user(),
        ]);
    }



    /****************************************************************************************************************
     *
     * Страница подробнее отчета
     *
     ****************************************************************************************************************/

    public function reportSingle(Report $id)
    {
        return view($this->path.'single_report', [
            'title' => 'Подробности отчета',
            'user' => Auth::user(),
            'report' => $id,
            'fields' => Report::fields()
        ]);
    }



    /****************************************************************************************************************
     *
     * Страница рейтинга
     *
     ****************************************************************************************************************/

    public function allCriterion($report_id)
    {
        return view($this->path.'all_criterion', [
            'title' => 'Все критерии оценки',
            'criterions' => Criterion::all(),
            'user' => Auth::user(),
            'report' => $report_id,
            'max_value' => Rating::getMaxValue(),
            'total' => Rating::getRating($report_id, Auth::user()->id),
            'other_ratings' => Rating::getOtherRatings()
        ]);
    }


    /****************************************************************************************************************
     *
     * Страница критериев для отчета
     *
     ****************************************************************************************************************/

    public function criterion($report, Criterion $criterion)
    {
        return view($this->path.'rating_report', [
            'title' => 'Оценка отчета',
            'user' => Auth::user(),
            'report' => $report,
            'criterion' => $criterion
        ]);
    }



    /****************************************************************************************************************
     *
     * Выставить оценку
     *
     ****************************************************************************************************************/


    public function estimate(Request $request)
    {
        $criterions = Criterion::all();
        $estimateCriterions = DB::select('SELECT total FROM middle_ratings WHERE report_id=? AND asessor_id=?', [
            $request->input('report_id'),
            $request->input('asessor_id')
        ]);

        if (Report::find($request->input('report_id'))->rating)
        {
            return redirect()->back();
        }

        if (count($criterions) == count($estimateCriterions))
        {
            $totals = 0.0;
            foreach ($estimateCriterions as $value)
            {
                $totals += $value->total;
            }

            Rating::create([
                'asessor_id' =>  $request->input('asessor_id'),
                'report_id' => $request->input('report_id'),
                'total_value' => $totals
            ]);

            return redirect()->route('asessor.reports')->with('success', 'Оценка по отчету выставлена');

        }

        return redirect()->back()->with('error', 'По данному отчету не выставленны все оценки');
    }

    /****************************************************************************************************************
     *
     * Сохраниение параметров критерия
     *
     ****************************************************************************************************************/

    public function saveCriterion(Request $request)
    {
        $request->validate([
            'param_1' => 'required|min:0|max:3|numeric',
            'param_2' => 'required|min:0|max:3|numeric',
            'param_3' => 'required|min:0|max:3|numeric',
            'param_4' => 'required|min:0|max:3|numeric',
            'best_practice' => 'required|min:0|max:0.2|numeric',
        ]);
        if (Report::find($request->input('report_id'))->rating)
        {
            return redirect()->back();
        }

        $isRating = Criterion::estimate($request->input('asessor_id'), $request->input('report_id'), $request->input('criteria_id'));

        if ($isRating)
        {
            $saved = MiddleRating::find($isRating->id);
            $saved->fill($request->all() + ['final' => false]);
            $saved->save();

            return redirect()->route('asessor.report.allcriterion', ['report_id' => $request->input('report_id')])->with('success', 'Данные успешно сохранены');
        }

        MiddleRating::create($request->all() + ['final' => false]);

        return redirect()->route('asessor.report.allcriterion', ['report_id' => $request->input('report_id')])->with('success', 'Данные успешно сохранены');

    }
}
