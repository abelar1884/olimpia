<?php

namespace App\Http\Middleware;

use Closure;

class Veryfied
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->active and $request->user()->role != 'admin')
        {
            return $next($request);
        }
        elseif ($request->user()->role == 'admin')
        {
            return redirect()->route('admin.index');
        }
        return redirect()->route('login')->with('error', 'Email не подтвержден');
    }
}
