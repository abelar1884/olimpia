<?php

namespace App\Http\Middleware;

use Closure;

class Asessor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->role == 'asessor')
        {
            return $next($request);
        }
        return redirect()->back();
    }
}
